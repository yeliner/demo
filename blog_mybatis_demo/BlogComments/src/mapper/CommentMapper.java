package mapper;

import java.util.List;

import bean.Author;
import bean.Blog;
import bean.Comment;

public interface CommentMapper {

	public void insertComment(Comment comment);

	/* 查询多个评论，不推荐使用 */
	public List<Comment> selectCommentsBybId(int blogId);
}
