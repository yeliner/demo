package bean;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

public class Blog {

	private int bId;
	private String title;
	private String content;
	private Author author;
	private String create_time;
	private String type;
	private List<Comment> comments;

	public List<Comment> getComments() {
		return comments;
	}
	public void setComments(List<Comment> comments) {
		this.comments = comments;
	}
	public Blog() {
		super();
	}
	public Blog(String title, String content, Author author, String create_time,
			String type) {
		super();
		this.title = title;
		this.content = content;
		this.author = author;
		this.create_time = create_time;
		this.type = type;
	}
	public int getbId() {
		return bId;
	}
	public void setbId(int bId) {
		this.bId = bId;
	}
	public Author getAuthor() {
		return author;
	}
	public void setAuthor(Author author) {
		this.author = author;
	}
	
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public String getCreate_time() {
		return create_time;
	}
	public void setCreate_time(String create_time) {
		this.create_time = create_time;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	
}
