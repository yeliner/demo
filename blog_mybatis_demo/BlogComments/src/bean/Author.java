package bean;

import java.io.Serializable;
import java.util.List;

public class Author  {

	private int aId;
	private String userName;
	private String psssWorld;
	private String email;
	private String adress;
	private String phone;
	private  List<Blog> blogs;
	public Author() {
		super();
	}
	public Author(String userName, String psssWorld, String email,
			String adress, String phone) {
		super();
		this.userName = userName;
		this.psssWorld = psssWorld;
		this.email = email;
		this.adress = adress;
		this.phone = phone;
	}
	public int getaId() {
		return aId;
	}
	public void setaId(int aId) {
		this.aId = aId;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getPsssWorld() {
		return psssWorld;
	}
	public void setPsssWorld(String psssWorld) {
		this.psssWorld = psssWorld;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getAdress() {
		return adress;
	}
	public void setAdress(String adress) {
		this.adress = adress;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public List<Blog> getBlogs() {
		return blogs;
	}
	public void setBlogs(List<Blog> blogs) {
		this.blogs = blogs;
	}
}
