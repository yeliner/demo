package com;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.apache.ibatis.session.SqlSession;

import bean.*;
import mapper.AuthorMapper;
import mapper.BlogMapper;
import mapper.CommentMapper;

public class Test {

	static SqlSession session = MybatisSqlSession.getSqlSession();
	static BlogMapper blogMapper = session.getMapper(BlogMapper.class);
	static AuthorMapper authorMapper = session.getMapper(AuthorMapper.class);
	static CommentMapper commentMapper = session.getMapper(CommentMapper.class);

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		// 添加数据
		//addInfo();
		// 根据博客id查找博客，并显示作者
		// selectBlogByBid();
		// 查询多条数据.根据作者查询有多少博客，利用属性,推荐使用
		// selectBlogsByAid();
		// 查询多条数据.根据博客查询有多少评论，直接查询
		// selectCommentsByBid();
		// 查询多条数据.根据博客查询有多少评论，利用属性
		// getCommentsByBid();
		// 根据博客属性来查找blog,联合查询
		// findBlogsByProperty();
		// 根据多个类型来查找blog
		// findBlogsByTypes();
		// 修改数据
		//mdfBlog();
		// 通过存储过程来查找Blog
		// getBlogByBidPro();
		// 通过存储过程来查找Blogs
		// getBlogsByAidPro();
		 

	}

	private static void getBlogsByAidPro() {
		HashMap<String, Object> ps = new HashMap<String, Object>();
		ps.put("aid", 1);
		List<Blog> bs = blogMapper.getBlogsByAidPro(ps);
		// assertEquals(2, ps.get("count"));
		int count=(Integer)ps.get("count");
		System.out.println(count);
		System.out.println(bs.size());
	}

	private static void getBlogByBidPro() {
		HashMap<String, Object> ps = new HashMap<String, Object>();
		ps.put("bid", 1);
		Blog b = blogMapper.getBlogByBidPro(ps);
		System.out.println(b.getTitle());
	}

	private static void mdfBlog() {
		selectBlogByBid();
		Blog updateBlog = new Blog();
		updateBlog.setTitle("萧敬腾来了");
		updateBlog.setContent("XXX");
		updateBlog.setbId(1);
		blogMapper.mdfBlog(updateBlog);
		session.commit();
		session.close();
		System.out.println("修改数据成功！");
	}

	private static void findBlogsByProperty() {
		// 根据博客属性来查找blog,联合查询
		Blog blog = new Blog();
		blog.setTitle("%天%");
		blog.setType("%天%");
		List<Blog> blogs = blogMapper.selectBlogsByProperty(blog);
		System.out.println(blogs.size());
	}

	private static void findBlogsByTypes() {
		// 根据多个类型来查找blog
		String[] types = { "心情", "天气" };
		List<Blog> blogs = blogMapper.selectBlogsByTypes(types);
		System.out.println(blogs.size());
	}

	private static void getCommentsByBid() {
		// 查询多条数据.根据博客查询有多少评论，利用属性
		System.out.println("查询多条数据.根据博客查询有多少评论，利用属性");
		Blog blog = blogMapper.getBlogBybId(1);
		List<Comment> comments = blog.getComments();
		System.out.println("作者是:" + blog.getAuthor().getUserName() + "\t博客标题是："
				+ blog.getTitle());
		System.out.println("评论：\t时间：\t");
		for (Comment comment : comments) {
			System.out.println(comment.getComment() + "\t"
					+ comment.getCreate_time());
		}
		System.out.println("共有" + comments.size() + "个评论");
	}

	private static void selectCommentsByBid() {
		// 查询多条数据.根据博客查询有多少评论，直接查询
		System.out.println("查询多条数据.根据博客查询有多少评论，直接查询");
		List<Comment> comments = commentMapper.selectCommentsBybId(1);
		// System.out.println("博客标题是："+comments.get(0).getBlog().getTitle());
		System.out.println("评论：\t时间：\t");
		for (Comment comment : comments) {
			System.out.println(comment.getComment() + "\t"
					+ comment.getCreate_time());
		}
		System.out.println("共有" + comments.size() + "个评论");
	}

	private static void selectBlogsByAid() {
		// 1.根据作者查询有多少博客，利用属性
		System.out.println("根据作者查询有多少博客，利用属性");
		Author alist = authorMapper.getAuthorByaId(1);
		List<Blog> blogs = alist.getBlogs();
		System.out.println("作者是：" + alist.getUserName());
		System.out.println("标题：\t内容：\t类型：\t");
		for (Blog blog : blogs) {
			System.out.println(blog.getTitle() + "\t" + blog.getContent()
					+ "\t" + blog.getType());
		}
		System.out.println("共有" + blogs.size() + "个博客");
	}

	private static void selectBlogByBid() {
		System.out.println("根据博客id查找博客，并显示作者");
		Blog selectBlog = blogMapper.getBlogBybId(1);
		System.out.println("博客标题:" + selectBlog.getTitle() + "\t作者:"
				+ selectBlog.getAuthor().getUserName());
	}

	private static void addInfo() {

		// 添加数据
		Author author = new Author("小黑", "123456", "xx.@123", "海边别墅", "x23456");
		authorMapper.addAuthor(author);
		Blog blog = new Blog("天晴了1", "下午天晴了", authorMapper.getAuthorByaId(1),
				"1997-11-29", "天气");
		blogMapper.addBlog(blog);
		Comment comment = new Comment(blogMapper.getBlogBybId(1), "好2",
				"2017-11-29");
		commentMapper.insertComment(comment);

		session.commit();
		session.close();
		System.out.println("添加数据成功!");

	}

}
