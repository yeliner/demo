create database MybatisBlogComments
use MybatisBlogComments

create table Author(
aId int identity(1,1) primary key,
userName varchar(50),
psssWorld varchar(50),
email varchar(100),
adress varchar(100),
phone varchar(20),
)

create table Blog(
bId int identity(1,1) primary key,
title varchar(50),
content  varchar(100),
authorId int references Author(aId),
create_time varchar(500),
[type] varchar(20)
)

create table Comments(
cId int identity(1,1) primary key,
blogId int references Blog(bId),
comment  varchar(100),
create_time varchar(500),
)
select * from author;
select * from blog;
select * from Comments;

--創建存儲過程,輸入參數
 CREATE PROCEDURE pro_getBlogByBid
 @bid int
as
begin
  select * from blog where bId=@bid;
end;

--執行存儲過程,輸入參數
declare @bid int
set @bid=2
exec pro_getBlogByBid @bid

----創建存儲過程,输出参数
 CREATE PROCEDURE pro_getBlogsByaId
 @aid int,
 @count int output
as
begin
 set @count=(select COUNT(*) from Blog where authorId=@aid);
 select @count
end;

drop PROCEDURE pro_getBlogsByaId
--執行存儲過程,输出参数
declare
 @aid int,
 @count int
set @aid=1
exec pro_getBlogsByaId @aid,@count
