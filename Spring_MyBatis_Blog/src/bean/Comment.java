package bean;

public class Comment {

	private int cId;
	private Blog blog;
	private String comment;
	private String create_time;
	public Comment() {
		super();
	}
	public Comment(Blog blog, String comment, String create_time) {
		super();
		this.blog = blog;
		this.comment = comment;
		this.create_time = create_time;
	}
	public int getcId() {
		return cId;
	}
	public void setcId(int cId) {
		this.cId = cId;
	}
	public Blog getBlog() {
		return blog;
	}
	public void setBlog(Blog blog) {
		this.blog = blog;
	}
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
	public String getCreate_time() {
		return create_time;
	}
	public void setCreate_time(String create_time) {
		this.create_time = create_time;
	}
	
}
