package mapper;

import java.util.HashMap;
import java.util.List;

import bean.Blog;

public interface BlogMapper {
	public void addBlog(Blog blog);

	public void delBlog(int blogId);

	public void mdfBlog(Blog blog);

	/* 查询博客，包括评论集合属性 */
	public Blog getBlogBybId(int blogId);
	//根据博客属性来查找blog
	public List<Blog> selectBlogsByProperty(Blog blog);
	//根据多个类型来查找blog
	public  List<Blog> selectBlogsByTypes(String[] types);
	
	public List<Blog> getBlogsByAidPro(HashMap<String, Object> ps);
	
	public Blog getBlogByBidPro(HashMap<String, Object> ps);
}
