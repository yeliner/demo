package mapper;

import java.util.List;

import org.apache.ibatis.annotations.Insert;

import bean.Author;
import bean.Blog;

public interface AuthorMapper {
	//利用注解添加
	@Insert("insert into Author(userName, psssWorld, email, adress,phone)"+
		"values(#{userName},#{psssWorld},#{email},#{adress},#{phone})")
	public void addAuthor(Author author);

	public void delAuthor(int authorId);

	public void mdfAuthor(Author author);

	/*查询作者，包括博客集合属性*/
	public Author getAuthorByaId(int authorId);
	

}
