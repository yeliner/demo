package impl;

import java.util.List;

import mapper.AuthorMapper;

import org.apache.ibatis.annotations.Insert;

import services.AuthorService;

import bean.Author;
import bean.Blog;

public class AuthorImpl implements AuthorService {
	private AuthorMapper authorMapper;

	// 利用注解添加
	public AuthorMapper getAuthorMapper() {
		return authorMapper;
	}

	public void setAuthorMapper(AuthorMapper authorMapper) {
		this.authorMapper = authorMapper;
	}

	@Insert("insert into Author(userName, psssWorld, email, adress,phone)"
			+ "values(#{userName},#{psssWorld},#{email},#{adress},#{phone})")
	public void addAuthor(Author author) {
		authorMapper.addAuthor(author);
	}

	public void delAuthor(int authorId) {
		authorMapper.delAuthor(authorId);
	}

	public void mdfAuthor(Author author) {
		authorMapper.mdfAuthor(author);
	}

	/* 查询作者，包括博客集合属性 */
	public Author getAuthorByaId(int authorId) {
		return authorMapper.getAuthorByaId(authorId);
	}

}
