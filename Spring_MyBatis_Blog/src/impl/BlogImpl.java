package impl;

import java.util.HashMap;
import java.util.List;

import mapper.BlogMapper;

import services.BlogService;

import bean.Blog;

public class BlogImpl implements BlogService {
	private BlogMapper blogMapper;

	public BlogMapper getBlogMapper() {
		return blogMapper;
	}

	public void setBlogMapper(BlogMapper blogMapper) {
		this.blogMapper = blogMapper;
	}

	public void addBlog(Blog blog) {
		blogMapper.addBlog(blog);
	}

	public void delBlog(int blogId) {
		blogMapper.delBlog(blogId);
	}

	public void mdfBlog(Blog blog) {
		blogMapper.mdfBlog(blog);
	}

	/* 查询博客，包括评论集合属性 */
	public Blog getBlogBybId(int blogId) {
		return blogMapper.getBlogBybId(blogId);
	}

	// 根据博客属性来查找blog
	public List<Blog> selectBlogsByProperty(Blog blog) {
		return blogMapper.selectBlogsByProperty(blog);
	}

	// 根据多个类型来查找blog
	public List<Blog> selectBlogsByTypes(String[] types) {
		return blogMapper.selectBlogsByTypes(types);
	}

	public List<Blog> getBlogsByAidPro(HashMap<String, Object> ps) {
		return blogMapper.getBlogsByAidPro(ps);
	}

	public Blog getBlogByBidPro(HashMap<String, Object> ps) {
		return blogMapper.getBlogByBidPro(ps);
	}
}
