package controller;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import entity.UserInfo;

import service.UserService;


@Controller
public class UserInfoController {
	@Resource
	private UserService userService;

	@RequestMapping("/add")
	public String add(UserInfo userInfo) {
		userService.addUserInfo(userInfo);
		return "redirect:/findAll";
	}

	@RequestMapping("/findAll")
	public String findAll(ModelMap dataMap) {
		List<UserInfo> userList = userService.findAll();
		dataMap.put("userList", userList);
		return "forward:/list.jsp";
	}

}
