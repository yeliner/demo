package service;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import entity.UserInfo;

import mapper.UserMapper;

@Service

public class UserService {
	@Resource
	private UserMapper userMapper;
	public void addUserInfo(UserInfo userInfo){
		userMapper.addUserInfo(userInfo);
	}
	public List<UserInfo> findAll(){
		return userMapper.findAll();
	}
}
