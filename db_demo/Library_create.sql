
--创建数据库
IF DB_ID( 'Library' )IS NOT NULL
	 DROP DATABASE Library

GO
CREATE DATABASE Library ON PRIMARY( 
	 NAME = 'Library' ,
		  FILENAME = 'D:\Yeliner\info\major\db\sql\Library.mdf' ,
		  SIZE = 10 ,
		  MAXSIZE = 100 ,
		  FILEGROWTH = 10)
	    log ON( 
		  NAME = 'BookSys_log' ,
		  FILENAME = 'D:\Yeliner\info\major\db\sql\Library.ldf' , 
		  SIZE = 10 , 
		  MAXSIZE = 100 ,
		  FILEGROWTH = 10 );

--创建登录名
create login LibAdmin with password='123', default_database=Library
--创建用户,并设置为数据库Library的dbo
create user LibAdmin for login LibAdmin with default_schema=dbo

use Library
--创建ReaderType表
		
if OBJECT_ID('TB_ReaderType') IS NOT NULL
    BEGIN
        DROP Table TB_ReaderType
    END;
Go
create table TB_ReaderType(
		  rdType   smallint  not  null  primary key,
		  rdTypeName   nvarchar (20) not null unique,
		  canLendQty   int,
		  canLendDay   int,
			canContinueTimes int,
			punishRate float,
			dateValid smallint default(0)--证书有效期（年）【非空，0表示永久有效】
	);


--创建TB_Reader表
		
if OBJECT_ID('TB_Reader') IS NOT NULL
    BEGIN
        DROP Table TB_Reader
    END;
Go
create table  TB_Reader (
		  rdID	 int not null primary key ,
			rdName  nvarchar (20),
			rdSex		nchar(1),
		  rdType  smallint not  null  references TB_ReaderType(rdType),		
		  rdDept  nvarchar(20),
			rdPhone nvarchar(25),
			rdEmail nvarchar(25),
			rdDateReg datetime,
			rdPhoto  image,
			rdStatus nchar(2),--证件状态，3个：有效、挂失、注销
		  rdBorrowQty int default 0 ,
			rdPwd nvarchar(20) default('123'),
			rdAdminRoles smallint, --管理角色，0-读者、1-借书证管理、2-图书管理、4-借阅管理、8-系统管理，可组合
			
	);

--创建Book表
		
if OBJECT_ID('TB_Book') IS NOT NULL
    BEGIN
        DROP Table TB_Book
    END;
Go
create table   TB_Book (
			bkID	 int  not null  identity(1,1)  primary key,
			bkCode nvarchar(20),--图书编号或条码号（前文中的书号）
			bkName nvarchar(50),
			bkAuthor  nvarchar(30),
			bkPress  nvarchar(50),
			bkDatePress datetime,
			bkISBN nvarchar(15),--ISBN书号
			bkCatalog nvarchar(30),--分类号（如：TP316-21/123）
			bkLanguage smallint,--语言，0-中文，1-英文，2-日文，3-俄文，4-德文，5-法文
			bkPages int,
			bkPrice  money,
			bkDateIn datetime,
			bkBrief text,
			bkCover image,
			bkStatus  nchar(2)--图书状态，在馆、借出、遗失、变卖、销毁
	);

--创建Borrow表
		
if OBJECT_ID('TB_Borrow') IS NOT NULL
    BEGIN
        DROP Table TB_Borrow
    END;
Go
create table   TB_Borrow (
			borrowID   numeric(12,0)  not null primary key ,
			rdID int not null references TB_Reader(rdID),
			bkID   int  not null references TB_Book(bkID),
			ldContinueTimes int,--续借次数（第一次借时，记为0）
			ldDateOut datetime,
			ldDateRetPlan datetime,
			ldDateRetAct datetime,
			ldOverDay int,
			ldOverMoney money,--超期金额（应罚款金额）
			ldPunishMoney money,--罚款金额
			lsHasReturn bit default 0 ,--是否已经还书，缺省为0-未还
			OperatorLend  nvarchar(20),
			OperatorRet  nvarchar(20)
	)


use Library

--添加读者类型
insert into TB_ReaderType	values(10,'教师',12,60,2,0.05,0);
insert into TB_ReaderType	values(20,'本科生',8,30,1,0.05,4);
insert into TB_ReaderType	values(21,'专科生',8,30,1,0.05,3);
insert into TB_ReaderType	values(30,'硕士研究生',8,30,1,0.05,3);
insert into TB_ReaderType	values(31,'博士研究生',8,30,1,0.05,4);

select * from TB_ReaderType
--添加读者
insert into TB_Reader(rdID,rdName,rdSex,rdType,rdDept,rdPhone,
rdEmail,rdDateReg,rdStatus,rdBorrowQty,rdPwd,rdAdminRoles)
 values(1,'admin','女','10','计科院','18942179081','123456@qq.com',GETDATE(),'有效',default,default,8),
 (2,'邱一','男','10','计科院','18942179082','123457@qq.com',GETDATE(),'有效',default,default,0), 
 (3,'邱二','女','20','艺术院','18942179083','123458@qq.com',GETDATE(),'有效',default,default,1)


select * from TB_Reader

insert into TB_Book(bkCode,bkName,bkAuthor,bkPress,bkDatePress,bkISBN,bkCatalog,bkLanguage,bkPages,bkPrice,bkDateIn,bkBrief,bkStatus)
 values ('BOOK1','UML','方梁','机械工业出版社',GETDATE()-100,'7-111-14518-6','TP312/12-3',0,100,59.0,GETDATE(),'入库第一本',default),
 ('BOOK2','UML','方梁','机械工业出版社',GETDATE()-100,'7-111-14518-6','TP312/12-3',0,100,59.0,GETDATE(),'暂无',default),
  ('BOOK3','UML','方梁','机械工业出版社',GETDATE()-100,'7-111-14518-6','TP312/12-3',0,100,59.0,GETDATE(),'机械工业教材',default),
	 ('BOOK4','C#','李铭','清华大学出版社',GETDATE()-100,'7-111-14518-6','TP412/12-3',0,100,119.0,GETDATE(),'清华大学出版',default),	 
	 ('BOOK5','C#','李铭','清华大学出版社',GETDATE()-100,'7-111-14518-6','TP412/12-3',0,100,119.0,GETDATE(),'清华大学出版',default),	 
	 ('BOOK6','C#','李铭','清华大学出版社',GETDATE()-100,'7-111-14518-6','TP412/12-3',0,100,119.0,GETDATE(),'清华大学出版',default),	 
	 ('BOOK7','SQL','王桃群','长江大学出版社',GETDATE()-100,'7-111-14518-6','TP412/12-3',0,100,119.0,GETDATE(),'长江大学出版',default)


 select * from TB_Book;
 
