--查询学生学号,姓名,考试科目名称及成绩
select student.stuNo,Subject.SubjectName,result.StudentResult
from Subject 
inner join Result  on Subject.SubjectId=Result.SubjectId
inner join Student on Result.StuNo=Student.stuNo

--查询参加'走进java编程世界'考试的学生姓名,成绩,考试日期
select student.stuName,Result.StudentResult,Result.ExamDate
from Result
inner join student  on Result.StuNo=Student.stuNo
inner join Subject on Subject.SubjectId=Result.SubjectId where Subject.SubjectName='走进java编程世界'
