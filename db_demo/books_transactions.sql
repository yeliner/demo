
use BooksDB
--1.自行举例说明自动提交事务与隐式事务的使用； 

--自动提交事务
 insert into Reader values('rd2018110',1,'邱二','计算机科学学院','9322666',default)

--隐式事务
 set implicit_transactions on
  insert into Reader values('rd2018111',1,'邱三','计算机科学学院','9322666',default)
 go

 select * from Reader

--2、改进借书存储过程，使用事务来保证数据的一致性。  
--存储过程,实现借书功能；
alter procedure usp_borrow
	@rdID char(9),
	@bkID char(9),
	@DateBorrow datetime,
	@DateLendPlan datetime
as
  begin tran
	print'begin tran'
	begin	try
	  --先判断图书状态
		declare @bkState int
		select @bkState=bkState from Book where bkID=@bkID
		if(@bkState!=1)
		  raiserror('借书失败!!该图书不在馆!',16,1)
		else
		  begin	
		--判断读者借书是否已满
			declare @rdBorrowQty int,@canLendQty int

			select @canLendQty=canLendQty,@rdBorrowQty=rdBorrowQty from Reader,ReaderType 
			where rdID=@rdID and Reader.rdType=ReaderType.rdType;

			if(@rdBorrowQty>=@canLendQty)
			   raiserror('借书失败!!该读者借书已达上限!',16,1)
		  else 
			    begin
				  insert into Borrow(rdID,bkID,DateBorrow,DateLendPlan) 
		      values(@rdID,@bkID,	@DateBorrow,@DateLendPlan);
			    --修改图书状态
					update Book set bkState=0 where bkID=@bkID;
					--修改借书数量
					update Reader set rdBorrowQty=rdBorrowQty+1 where rdID=@rdID;
					print @rdID+'借'+@bkID+'成功!!'
					commit
					print'commit'
		      end
		  end
	end try
	begin catch
	  DECLARE @ErrorMessage NVARCHAR(4000)
	  select @ErrorMessage = ERROR_MESSAGE()
	  raiserror(@ErrorMessage,16,1)
		rollback
		print'rollback'
	end catch


--测试执行
declare @date datetime,@date_plan datetime
--获取当前时间为借书时间
set @date=GETDATE()
--计划还书时间为借书后的第十天
set @date_plan=GETDATE()+10 
exec usp_borrow'rd2018001','bk2018001',@date,@date_plan




select Borrow.rdID, Borrow.bkID,DateBorrow,DateLendPlan,rdBorrowQty, bkState from Borrow,Book,Reader 
where Borrow.rdID=Reader.rdID and Borrow.bkID=Book.bkID


--3、新建两个登录帐户（帐户名：wtq，密码 wtq888； 帐户名：test，密码：test888）
--其中，帐户 wtq可对BooksDB数据库中的ReaderType表进行SELECT、 INSERT操作，但无权进行DELETE、 UPDATE 操作。
--帐户 test 无权访问 BooksDB 数据库。 
--创建登录用户
create login wtq with password='wtq888', default_database=BooksDB
create login test with password='test888', default_database=BooksDB

--创建用户
create user wtq for login wtq 
create user test for login test 
--授权
grant select,insert on  ReaderType to wtq
--撤权



--4、分别使用SSMS与SQL命令实现此功能：
-- 授予帐户wtq 对ReaderType表进行DELETE、 UPDATE 操作的权限

--sql语句
grant delete,update on  ReaderType to wtq