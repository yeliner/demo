--查询学生姓名,所属年级名及联系电话
select 学生姓名=student.stuName,所属年级名称=subject.SubjectName , 联系电话=student.Phone 
from Student,Subject
where Student.stuGradeId=Subject.GradeId

--查询年级编号为1的年级名称,科目名称及学时
select 年级名称=Grade.GradeName,科目名称=subject.SubjectName
from Grade,Subject
where grade.GradeId=Subject.GradeId and  grade.GradeId=1

--查询参加科目编号为1的考试学生姓名,分数,考试日期
select Student.stuName,result.StudentResult,Result.ExamDate
from Student,Result
where student.stuNo=result.StuNo and  Result.SubjectId=1

--查询学号为20131101007的学生参加的考试科目名称,分数,考试日期
select subject.SubjectName,result.StudentResult,Result.ExamDate
from Subject
inner join Result on Subject.SubjectId=Result.SubjectId
 where Result.StuNo=20131101007

--查询参加考试的学生学号,所考科目名称,分数,考试日期
select result.StuNo,Subject.SubjectName,result.StudentResult,Result.ExamDate
from Result 
inner join Subject on Subject.SubjectId=Result.SubjectId