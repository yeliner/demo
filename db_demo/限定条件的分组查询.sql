--查询每学年学时超过50的课程数
select COUNT(SubjectId) from Subject group by GradeId having SUM(ClassHour)>50

--查询每学年学生的平均年龄
select AVG(DATEDIFF(yy,borndate,getdate()))  from Student group by stuGradeId 

--查询北京地区的每学年学生人数
select COUNT(*) from Student where stuAdrress like '%山东%' group by stuGradeId 

--查询参加考试的学生中,平均分及格的学生记录,并按照成绩降序排序
select stuno as 学号, 平均分=AVG(StudentResult) from Result group by StuNo having AVG(StudentResult)>60 order by AVG(StudentResult)desc

--查询考试日期为2013,3,22日的课程的及格平均分
select  平均分=AVG(StudentResult)  from Result where ExamDate='2013-3-22'  group by SubjectId

--查询至少一次考试不及格的学生考号,不及格次数
select StuNo , count(stuno) from Result where StudentResult<60 group by StuNo 