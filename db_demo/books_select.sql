use BooksDB
select * from Reader
--1、单表查询 
--（1）	查询所有读者的编号和姓名； 
select rdID 编号,rdName 姓名 from Reader

--（2）	查询所有读者的编号、姓名和单位，要求修改查询结果的列名； 
select rdID 编号,rdName 姓名,rdDept 单位 from Reader

--（3）	查询 Reader 表的全部列 
select * from Reader

--（4）	查询借阅过图书的读者的编号； 
select distinct  rdID  读者编号 from Borrow

--（5）	查询单价大于 30 元的图书的书号和书名； 
select bkID as 书号,bkName 书名 from Book where bkPrice>30

--（6）	查询单价不在 30 至 40 元之间的图书的书号、书名和作者； 
select  bkID as 书号,bkName 书名,bkAuthor 作者  from Book where bkPrice not between  30 and 40

--（7）	查询既不是管理学院、也不是物理学院的读者的姓名和 QQ； 
select rdName,rdQQ from Reader where rdDept not in('管理学院','物理学院')

--（8）	查询所有姓“王”的读者的姓名、单位和 QQ； 
select rdName,rdDept,rdQQ from Reader where rdName like ('王%')

--（9）	查询所有不姓“王”的读者的姓名、单位和 QQ； 
select rdName,rdDept,rdQQ from Reader where rdName not like ('王%')

--（10）	查询所有全名只有两个字的读者的姓名、单位和 QQ； 
select rdName,rdDept,rdQQ from Reader where len(rdName)=2
select rdName,rdDept,rdQQ from Reader where rdName  like ('__')


--（11）	查询所有图书还未归还的借阅信息； 
select * from Borrow where DateLendAct is null

--（12）	查询借阅了书号“bk2018001”的读者的编号和借书日期， 查询结果按借书日期降序排列； 
select rdID,DateBorrow from Borrow where bkID='bk2018001'order by DateBorrow desc

--（13）	查询读者的总人数； 
select COUNT(*) 读者总人数 from Reader

--（14）	查询借阅过图书的读者人数； 
select count(distinct rdID ) from Borrow

--（15）	查询所有图书的最高单价； 
select max(bkPrice) from Book

--（16）	查询各单位名及该单位的读者人数； 
select rdDept,COUNT(rdID) 读者人数 from Reader group by rdDept

--（17）	查询读者人数大于 40 的单位名及该单位的读者人数； 
select rdDept,COUNT(rdID) 读者人数 from Reader group by rdDept having COUNT(rdID)>40


--2、连接查询 

--（18）	查询类别号为 1 的所有读者的姓名和单位； 
select rdName,rdDept from Reader where rdType=1
select rdName,rdDept from Reader,ReaderType where ReaderType.rdType=1

--（19）	查询'管理学院'所有读者的编号、姓名和可借书数量； 
select R.rdID,rdName,canLendQty from Reader R,ReaderType RT
 where rdDept='管理学院'and R.rdType=RT.rdType


--（20）	查询借阅了书号“bk2017001”的读者的姓名、可借书数量和可借书天数； 
select R.rdID,rdName,canLendQty,canLendDay from Reader R,ReaderType RT
 where R.rdType=RT.rdType and  R.rdID in (
select rdID from Borrow B where  B.bkID='bk2018001')


--（21）	查询每个读者及其借阅信息的情况（即使没有借过书，也列出该读者的基本信息）； 
select R.rdID,rdType,rdName,rdDept,rdQQ,rdBorrowQty,bkID,DateBorrow,DateLendPlan,DateLendAct  
from  Reader R left outer join Borrow B on R.rdID=B.rdID


--3、嵌套查询 

--（22）	查询与连晓燕在同一个单位的读者； 
select * from Reader R
 where R.rdDept in (
select rdDept from Reader where R.rdName='连晓燕')

--（23）	查询借阅了书名为“高等数学”的读者的编号和姓名；
select R.rdID,rdName from Reader R 
where R.rdID in(
select rdID from Borrow where bkID in(
select bkID from Book where bkName='高等数学') )


--（24）	查询所有没借阅过书号为“bk2017004”的读者姓名； 
select rdName from Reader
 where rdID not in(
select rdID from Borrow where bkID='2018004')

--4、集合查询 
--（25）	查询管理学院的读者及借书本数小于 4 的读者； 
select * from Reader where rdDept='管理学院'
 union 
 select * from reader where rdid in(select rdID from Borrow group by rdID having COUNT(bkID) <4)


--（26）	查询管理学院借书本数小于 4 的读者； 
select * from Reader where rdDept='管理学院' 
intersect
 select * from reader where rdid in( select rdID from Borrow  group by rdID having COUNT(bkID) <4)


--（27）	查询管理学院借书本数大于 4 的读者； 
 select * from Reader where rdDept='管理学院'
  except 
	select * from reader where rdid in( select rdID from Borrow  group by rdID having COUNT(bkID) <4)

--5、数据更新 

--（28）	将一个新读者记录(读者编号：2017007；类别：1， 姓名：卢小川，单位：计算机科学学院，QQ：932200777， 已借书数量：0）插入 Reader 表中 
insert into Reader(rdID,rdType,rdName,rdDept,rdQQ,rdBorrowQty)
 values('2018007',1,'卢小川','计算机科学学院',932200777,0);


--（29）	插入一条借阅记录（读者编号：2017007， 书号：bk2017004， 借书日期：2017-3-8）插入Borrow 表中； 
insert into Borrow(rdID,bkID,DateBorrow) 
values('2018007','bk2017004','2018-3-8')

--（30）	将读者 rd2017001 的 QQ 号修改为：3635752； 
update Reader set rdQQ='3635752' where rdID='rd2018001'

--（31）	将读者 rd2017001 的类别号修改为 2；
update   Reader set rdType=2 where rdID='rd2018001'

--（32）	删除所有的借阅记录
delete Borrow
