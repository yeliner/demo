<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=basePath%>">

<title>My JSP 'list.jsp' starting page</title>

<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
<meta http-equiv="description" content="This is my page">
<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->

<script type="text/javascript" src="angular.min.js"></script>
<script type="text/javascript" src="controller.js"></script>
</head>

<body ng-app="find_app" ng-controller="find_c">
	<div align="center">
		查询条件: <input type="text" name="name" ng-model="name" /> <input
			type="submit" value="点击查询" ng-click="submit()" />
	</div>
	<table align="center">
		<tr>
			<td>Id</td>
			<td>用户名</td>
			<td>密码</td>
		</tr>
		<tr ng-repeat="x in users">
			<td>{{x.id}}</td>
			<td>{{x.name}}</td>
			<td>{{x.pwd}}</td>
		</tr>
	</table>

</body>
</html>
