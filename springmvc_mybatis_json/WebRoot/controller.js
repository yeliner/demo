var add_app = angular.module("add_app", []);
add_app.controller("add_c", function($scope, $http) {

	$scope.submit = function() {
		var postData = "?name=" + $scope.name + "&pwd=" + $scope.pwd;
	
		var url = "add" + postData;
		$http.post(url, postData).success(function(Data) {
			$scope.user=Data;
		});

	};
});

var find_app = angular.module("find_app", []);
find_app.controller("find_c", function($scope, $http) {

	$scope.submit = function() {
		var postData = "?name=" + $scope.name;
		var url = "findAll" + postData;
		$http.post(url, postData).success(function(Data) {
			$scope.users = Data;
		});

	};
});