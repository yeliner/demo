package service;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import entity.UserInfo;

import mapper.UserMapper;

@Service

public class UserService {
	@Resource
	private UserMapper userMapper;
	public boolean addUserInfo(UserInfo userInfo){
		try {
			userMapper.addUserInfo(userInfo);
			return true;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
	
	}
	public List<UserInfo> findAll(){
		return userMapper.findAll();
	}
}
