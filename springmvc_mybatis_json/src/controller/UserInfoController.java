package controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONArray;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import com.google.gson.Gson;
import com.google.gson.JsonArray;

import entity.UserInfo;

import service.UserService;

@Controller
public class UserInfoController {
	@Resource
	private UserService userService;

	@RequestMapping("/add")
	public void add(HttpServletResponse res, UserInfo userInfo) {
		String str = "fail";
		if (userService.addUserInfo(userInfo)) {
			Gson gson = new Gson();
			str = gson.toJson(userInfo);
		}
		PrintWriter writer;
		try {
			res.setContentType("text/json;charset=utf-8");
			writer = res.getWriter();
			writer.print(str);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@RequestMapping("/findAll")
	public void findAll(HttpServletResponse res) {
		List<UserInfo> userList = userService.findAll();
		JSONArray jsonArray = JSONArray.fromObject(userList);
		PrintWriter writer;
		try {

			res.setContentType("text/json;charset=utf-8");
			writer = res.getWriter();
			writer.print(jsonArray);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
