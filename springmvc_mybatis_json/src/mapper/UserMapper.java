package mapper;

import java.util.List;

import entity.UserInfo;

public interface UserMapper {
	public void addUserInfo(UserInfo userInfo);
	public List<UserInfo> findAll();

}
