#pragma once
#pragma once
//判断练习
void sort_xyz(int x, int y, int z);//xyz从小到大排序,并输出

int ispalindrome(long int x);//判断回文数 第4第6

double gettax(double income);//计算税

char toupper(char a);//大写

int isodd(int a);//判断奇偶数

void switch1();//使用switch判断等级

 //循环示例		
void multable();//打印乘法表

void multable2();

void qdivisor(int n);//分解质因子,并输出

int isprime(int n);//判断素数

void printprime(int m, int n);//求m-n以内的素数,并输出


//累加和,连积

float fac(int n);//阶乘

float getfac_s(int n);//计算s=1!+(2)!+(3)!+...+n!

float getfac_s(int m, int n);//计算s = n!+ (n + 1)!+ (n + 2)!+ ... + m!

float getfac_e(int n);//计算e=1+1/1!+1/2!+...+1/n!;求阶乘的倒数之和


 //穷举法
void printchicken();//百钱买百鸡,公鸡3,母鸡5,鸡仔1/3

void getstep();//爱因斯坦阶梯问题

void steel();//钢管

int getgcd(int m, int n);

int getlcm(int m, int n);

int getgcd(int x, int y, int z);//求最大公约数

int getlcm(int x, int y, int z);//最小公倍数


 //递推法
int tao(int n);//猴子吃桃递归公式函数

void peach();//猴子吃桃,利用递归,打印结果

void peach2();

int fib(int n);//斐波那契数列递归函数

void printfib(int n);//打印斐波那契数列

void printfib2(int n);//

float getpi4();//计算pi/4=1-1/3+1/5-1/7

double getpi2();//计算pi/2=1+1/3+1/3*2/5+...第6章编程题

//第五章编程题
int isnar(int n);//判断水仙花数

void printnar();//打印水仙花数

int isleapyear(int i);//判断闰年

void printleapyear(int m, int n);//判断m-n之间得闰年年号,并5个年号换行

int getseries1(int n);//计算1-3+5-7+...-99+101;规律题1 正负奇数交叉求和

float getseries2(int n);//计算Sn=2/1+3/2+5/3+8/5+...,规律题2,前n项分式之和

float getseries3(int n);//计算sum=1+(1+1/2)+(1+1/2+1/3)+...,规律题3

float getfall_s(int n);//小球第n次路径

float formula(float x);//牛顿迭代公式求高次方程的根

void pfraction();//ab/bc=a/c,特殊真分数

//数组练习
void printscore();//成绩数组

void printn_n();//N*N矩阵,输出主次对角线元素之和

void getmax_option();//找最大值的位置

void yht();//杨辉三角

void addnum(int n);//插入元素

void delnum(int i);//删除指定位置元素

void inv_row_colum();//交换行和列

void printarray2();//打印矩阵

void printa_b();//打印A*B?


//函数练习
void sort(int a[]);//使用选择法,从小到大排序

void inv(int a[], int n);//将一个数组中的n个数据按颠倒的顺序重新存放

void inv2(int a[], int n);


//嵌套函数

double inteq(double a, double b);//积分上限,积分下限,求s=f(a+i*h)

double fd(double x);//sinx函数

void countch(char a[], int b[]);//统计一行字符中,出现的每个小写字母的个数

float get_t_s(float a, float b, float c);//求三角形的面积

float getseries5(int m);//计算t=1-1/2*2-1/3*3-...-1/m*m

int reverse(int m);//尾数非0的正整数的逆序数

void getcount(char s[]);//统计字符,空格,字母,其他


//位运算
void printbin(short int num);//打印数字的二进制

void printbin8_11(short int num);//取二进制的8-11位构成的数

void printbinex(unsigned short  a, int n);//将a循环右移n个数据

//指针
void dynamicarray();//动态分配一段内存空间用于存储一个可变大小的整型数组

void printarray();//一维数组与指针,访问第i个元素的几种方法

void swap(int *px, int *py);

void invarray(int a[], int n);//将数组顺序颠倒

void bubarray(int a[], int start, int end);//实现将数组任意指定范围内的数据按从小到大

void bubarray2(int a[], int n);//用指针实现,将数组任意指定范围内的数据按从小到大
void bubarray1(int a[], int n);
void bubarray4(int a[], int n);

void prnArray(int a[], int num);

void cpystr(const char * s, char str[]);//将字符串s的内容拷贝到字符数组str中

int cmpstr(char * str1, char * str2);//两个字符相比较

void yhtp();//指针杨辉三角

void yhtp2();//

//指针练习
void compute(int m, int n, int*sum, int*p);//计算m,n的和;m,n的积,并通过参数返回

void subnum(char s[81]);	//对字符串提取数字字符

void sortpeople(int n, int start, int a, int b);//n个人围一个圈