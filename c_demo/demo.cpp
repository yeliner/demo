
#include <stdio.h>
#include <math.h>
#include <string.h>
#include <stdlib.h>
//判断
void sort_xyz(int x, int y, int z)
{
	//xyz从小到大排序
	int t;
	if (x > y)
	{
		t = x;
		x = y;
		y = t;
	}
	if (x > z)
	{
		t = x;
		x = z;
		z = t;
	}
	if (y > z)
	{
		t = y;
		y = z;
		z = t;
	}
	printf("%d\t%d\t%d\n", x, y, z);
}

int ispalindrome(long int x) {
	//判断回文数12321
	//先判断是几位数,n=5
	int n = 0, t = x, h, l;
	while (x)//==>x!=0时
	{
		n++;
		t = t / 10;//判断位数
	}
	while (x)
	{
		h = x / (int)pow(10, n - 1);//取最高位,h=12321/(10*10*10*10)=1
		l = x % 10;//最低位
		if (h != l) {
			return 0;//不是回文数

		}
		else {
			//继续比较
			x = x % (int)pow(10, n - 1);//去掉最高位 x=2321
			x = x / 10;//去掉最低位,x=232
			n = n - 2;//位数减2		
		}
	}
	return 1;
}

double gettax(double income)
{
	//计算税
	double tax;
	if (income < 3500)
	{
		tax = 0;
	}
	else if (income <= 5000)
	{
		tax = (income - 3500)*0.03;
	}
	else
	{
		tax = (5000 - 3500)*0.03 + (income - 5000)*0.1;
	}
	return tax;
}

char toupper(char a)
{
	//转大写字母
	if ((a >= 97) && (a <= 122))
	{
		a = a - 32;
	}
	return a;
}

int isodd(int a)
{
	//判断奇偶数	
	if (a % 2 == 0)
		return 1;
	return 0;
}

void switch1()
{
	//将if改成switch
	int a, m;
	printf("input a:");
	scanf_s("%d", &a);
	switch (a / 10)
	{

	case 0:
	case 1:
	case 2:
	case 3:
	case 4:
	case 5:m = 1; break;
	case 6:m = 2; break;
	case 7:m = 3; break;
	case 8:m = 4; break;
	default:
		m = 5; break;

	}
	printf("m=%d:", m);
}

//循环示例

void qdivisor(int n)
{
	//分解质因子,并输出
	int i = 2;
	printf("%d=", n);
	while (n > 1) {
		while (n%i == 0)
		{
			n = n / i;//相同质因子可能有多个
			printf("%d%c", i, n > 1 ? '*' : '\n');
		}
		i++;
	}
}

void multable()
{
	//99乘法表
	int i, j;

	for (i = 1; i <= 9; i++)
	{
		for (j = 1; j <= i; j++)
		{
			printf("%ld*%ld=%2d\t", j, i, i*j);
		}
		printf("\n");
	}
}

void multable2() {
	int i, j;//定义i,j
	for (i = 1, j = 1; i <= 9; ) {//i控制行,j控制列
		printf("%d*%d=%d\t",j,i,j*i);//先打印
		j++;
		if (j>i) {//当j>i的时候,说明改换行了,i++,并且j也应该重置
			printf("\n");
			i++;
			j = 1;
		}
	}
}

void printprime(int m, int n) {
	//求m-n以内的素数,并输出
	int isprime(int n);//声明

	while (m <= n)
	{
		if (isprime(m)) {
			printf("%5d", m);
		}
		m++;
	}
}

int isprime(int n) {
	//判断素数,素数只能被1和本身整除
	int i = 2;
	//素数肯定为奇数,或者i<=sqrt(n);
	while (i <= n / 2)
	{
		if (n%i == 0) {
			return 0;
			break;
		}
		i = i + 1;
	}
	return 1;
}


//累积法
float fac(int n)
{
	//求n!=n*(n-1)*...*1;
	if (n == 1 || n == 0)
	{
		return 1;
	}
	return n * fac(n - 1);
}

float getfac_s(int n)
{
	//计算s=1!+(2)!+(3)!+...+n!
	float fac(int n);//声明
	float sum = 0;
	int i;
	for (i = 1; i <= n; i++)
	{
		sum += fac(i);
	}
	return sum;
}

float getfac_s(int m, int n)
{
	//计算s=n!+(n+1)!+(n+2)!+...+m!
	int i;
	float s = 1, sum = 0;
	for (i = 1; i <= m; i++)
	{
		s = s * i;
		if (i >= n)
		{
			sum += s;
		}
	}
	return sum;
}

float getfac_e(int n) {
	//分式求值,记得要用浮点型
	//e=1+1/1!+1/2!+...+1/n!;
	int i;
	float e = 1, t;//项
	for (i = 1; i <= n; i++)
	{
		t = float(1.0) / fac(i);
		e += t;
	}
	return e;
}

//穷举法,枚举法,试凑法
void printchicken()
{
	//百钱买百鸡,公鸡3,母鸡5,鸡仔1/3
	int x, y = 0, z;
	for (x = 0; x <= 33; x++) {
		for (z = 0; (z + x) <= 100; z += 3)
		{
			y = 100 - x - z;
			if (3 * x + 5 * y + z / 3 == 100)
			{
				printf("x=%d,y=%d,z=%d\n", x, y, z);

			}
		}
	}
}

void getstep() {
	//爱因斯坦阶梯问题,每步跨2阶,余1;跨3余2;跨5余4;跨6余5;跨7余0;
	int x = 7;
	while ((x % 3 != 2) || (x % 5 != 4) || (x % 6 != 5))
	{
		x = x + 14;//肯定是奇数
	}
	printf("x=%d\n", x);
}

void steel() {
	//钢管:323米,截取成a:17米;b:27米两种,那种方法剩余材料r最少?
	int min, r, L = 323, x, y, count_a = 1, count_b = 1;
	min = L - 17 - 27;//先假设ab各一段的剩余材料最少;
	for (x = 1; x <= (L - 27) / 17; x++)
	{
		y = (L - 17 * x) / 27;//计算b的段数
		r = L - x * 17 - y * 27;
		if (r < min)
		{
			min = r;
			count_a = x;
			count_b = y;

		}

	}
	printf("a:%d\tb:%d\t min=%d\n", count_a, count_b, min);
}

int getgcd(int m, int n) {
	//Greatest common divisor最大公约数	
	//取最小值
	int r;
	r = m % n;
	while (r != 0)
	{
		//循环整除
		m = n;
		n = r;
		r = m % n;
	}
	return n;
}

int getlcm(int m, int n) {
	//最小公倍数=m*n/最大公约数
	return m * n / getgcd(m, n);
}

int getgcd(int x, int y, int z) {
	//Greatest common divisor最大公约数	
	//最大公约数<=最小值
	//取最小值
	int r;
	r = x > y ? ((y > z) ? z : y) : (x > z ? z : x);
	while (r > 1)
	{
		//循环比最小值小的数,依次判断是否可以被整除
		if (x%r == 0 && y%r == 0 && z%r == 0) {
			break;
		}
		r--;
	}

	return r;
}

int getlcm(int x, int y, int z) {
	//lowest common multiple 最小公倍数	
	//最小公倍数>=最大值
	//取最大值
	int r;
	r = x > y ? (x > z ? x : y) : ((y > z) ? y : z);
	while (r)
	{
		//循环比最小值小的数,依次判断是否可以被整除
		if (r%x == 0 && r%y == 0 && r%z == 0) {
			break;
		}
		r++;
	}
	return r;
}


//递推法,迭代法
int tao(int n) {
	//f(n)=2*f(n+1)+1,f(8)=1;
	if (n == 8)
		return 1;
	return 2 * (tao(n + 1) + 1);
}

void peach() {
	//打印结果
	////猴子吃桃,某天摘若干个,当天吃掉一半,又多吃一个重复到第8天时只剩下一个桃子了
	//Xn=(Xn-1)/2-1;Xn=2*((Xn+1)+1),X8=1;
	int tao(int n);//声明
	int n;
	for (n = 1; n <= 8; n++)
	{
		printf("第%d天有%d个桃子\n", n, tao(n));
	}

}

void peach2() {
	//猴子吃桃,某天摘若干个,当天吃掉一半,又多吃一个重复到第8天时只剩下一个桃子了
		//Xn=(Xn-1)/2-1;Xn=2*((Xn+1)+1),X8=1;
	int tao, n;
	tao = 1;
	for (n = 7; n >= 1; n--)
	{
		tao = (tao + 1) * 2;

	}
	printf("tao(1)=%d\n", tao);

}


int fib(int n) {
	//递推公式,f(n)=f(n-1)+f(n-2);
	if (n = 2 || n == 1)
		return 1;
	return fib(n - 1) + fib(n - 2);
}

void printfib(int n) {
	//斐波那契数列,第一第二项为1,从第三项起,是前两项的值之和.
	//递推公式,f(n)=f(n-1)+f(n-2);
	int fib(int n);//声明
	int i;
	for (i = 1; i <= n; i++)
	{
		printf("%d%c", fib(i), i % 5 == 0 ? '\n' : '\t');
	}
}

void printfib2(int n) {
	int i, f1 = 1, f2 = 1, f3;
	printf("%d\t%d\t", f1, f2);
	for (i = 3; i <= n; i++)
	{
		f3 = f1 + f2;
		printf("%d\t", f3);
		if (i % 5 == 0)
			printf("\n");
		f1 = f2;
		f2 = f3;
	}
}

float getpi4() {
	//分式运算,记得用浮点型
	//求pi得近似值,pi/4=1-1/3+1/5-1/7
	int fm = 1;//分母值
	float t, fz, pi;
	fz = 1.0;//分子值
	t = fz / fm;//项
	pi = 0;
	do
	{
		pi += t;
		fz *= -1;
		fm += 2;
		t = fz / fm;//项
	} while (fabs(t) >= 0.00001);
	return pi * 4;
}

double getpi2() {
	//pi/2=1+1/3+1/3*2/5+...
	int n;
	double pi = 1, t = 1;
	n = 1;
	do {
		t = t * n / (2 * n + 1);
		pi = pi + t;
		n++;
	} while (t > 1e-5);
	return 2 * pi;
}


//第5章练习,第六章
int isnar(int n) {
	//Narcissus 水仙花
	//判断水仙花数,三位数其各位数得立方之和等于该数本身
	int  x, y, z;
	x = n / 100;
	y = n / 10 % 10;
	z = n % 10;
	if (n == (x*x*x + y * y*y + z * z*z))
		return 1;
	else
		return 0;
}

void printnar() {
	//打印水仙花数
	for (int i = 999; i >= 100; i--) {
		if (isnar(i)) {
			printf("%d\t", i);
		}
	}
}

int isleapyear(int i) {
	//年号能被4整除,但不能被100整除,或者年号能被400整除
	if ((i % 4 == 0 && i % 100 != 0) || i % 400 == 0)
		return 1;
	else
		return 0;
}

void printleapyear(int m,int n) {
	//判断m-n之间得闰年年号,并5个年号换行	
	int isleapyear(int i);//声明
	int i, count = 0;
	for (i = m; i <= n; i++)
	{
		if (isleapyear(i))
		{
			count++;
			printf("%d%c", i, count % 5 == 0 ? '\n' : '\t');
		}
	}

}

int getseries1(int n) {
	//计算1-3+5-7+...-99+101;规律题1 正负奇数交叉求和
	int i, t = 1, sum = 1;
	if (n%2==0)
	{
		return 0;
	}
	for (i = 3; i <= n; i += 2) {
		t = -t;
		sum = sum + t * i;
	}
	return sum;
}

float getseries2(int n) {
	//求Sn=2/1+3/2+5/3+8/5+...
	//分式运算,记得用浮点型
	int fz, fm, i;
	float s, t;
	fz = 2;
	fm = 1;
	s = 0;
	for (i = 1; i <= n; i++)
	{
		t = (float)(1.0*fz) / fm;
		s = s + t;
		fz = fz + fm;
		fm = fz - fm;
	}
	return s;
}

float getseries3(int n) {
	//计算sum=1+(1+1/2)+(1+1/2+1/3)+...
	float s, t;
	int i;
	s = 0, t = 0;
	for (i = 1; i <= n; i++)
	{
		t = t +(float)1.0 / i;
		s = s + t;
	}
	return s;
}

float getfall_s(int n) {
	//从80米处自由落下,落地后反复弹起,每次弹起的高度都是上次高度的一半,求第n次落地的而总路程
	float sum, h;
	int i;
	h = 40.0;
	sum = 80.0;
	for (i = 2; i <= n; i++)
	{
		sum += 2 * h;
		h = h / 2;
	}
	return sum;

}

float formula(float x)
{
	//牛顿迭代公式求高次方程的根
	// X 的初值可为任意值
	float f, f1;
	do
	{
		f = 2 * x*x*x - 4 * x*x + 5 * x - 18;//高次方程f(x)
		f1 = 6 * x*x - 8 * x + 5;//f'(x)导函数在x1处的值
		x = x - f / f1;//牛顿迭代公式x2=x1-f(x1)/f'(x1)
	} while (fabs(f) > 0.00001);
	//printf("x=%.8f,f=%f\n", x, f);
	return x;
}

void pfraction() {
	//proper fraction 真分数
	//分子与分母都是两位整数，分子个位与分母十位相同，分子去掉个位，分母去掉十位，与原值相同
	int a, b, c;//ab/bc=a/c
	for (a = 1; a <= 9; a++)
		for (b = 1; b <= 9; b++)
			for (c = 1; c <= 9; c++) {
				if (a < c && (1.0*a / c == (a*10.0 + b) / (b*10.0 + c))) {
					printf("%d/%d=%d%d/%d%d=%f\n", a, c, a, b, b, c, 1.0 *a / c);
				}
			}
}


//数组
void printscore() {
	//学生成绩
	int i, count = 0, n;
	float a[40], max = 0, min = 100.0, sum = 0, avg = 0;
	printf("Enter student number:");
	scanf_s("%d", &n);
	if (n > 0)
	{
		printf("please input student score by '\n' end:\n");
		for (i = 0; i < n; i++)
		{
			scanf_s("%f\t", &a[i]);
			if (fabs(a[i]) < min)
			{
				min = a[i];
			}
			if (fabs(a[i]) > max)
			{
				max = a[i];
			}
			sum += a[i];
		}

		avg = sum / n;
		for (i = 0; i < n; i++)
		{
			printf("%6.2f\t", a[i]);
			if (a[i] >= avg)
			{
				count++;
			}
		}
		printf("\nmax=%6.2f,min=%6.2f,avg=%6.2f,count=%d\n", max, min, avg, count);
	}
	else
	{
		printf("number of student is 0!\n");
	}
}

void printn_n()
{
	//N*N矩阵,主次对角线元素之和
	int a[3][3], i, j, s1 = 0, s2 = 0;
	printf("please input number by \\n to end:\n");
	for (i = 0; i < 3; i++)
	{
		for (j = 0; j < 3; j++)
		{
			scanf_s("%d\t", &a[i][j]);
			if (i == j)
			{
				s1 += a[i][j];
			}
			if ((i + j) == 2)
			{
				s2 += a[i][j];
			}
		}
	}
	printf("s1=%d,s2=%d\n", s1, s2);
}

void getmax_option()
{
	//找最大数的位置
	int a[3][4] = { { 1,2,3,4 },{ 9,8,7,6 },{ -10,10, -5, 2 } };
	int i, j, row, colum, max = a[0][0];
	for (i = 0; i < 3; i++)
	{
		for (j = 0; j < 4; j++)
		{
			printf("%d\t", a[i][j]);

			if (max < a[i][j])
			{
				max = a[i][j];
				row = i;
				colum = j;
			}
		}
	}
	printf("\nmax=%d,row=%d,colum=%d\n", max, row, colum);
}

void yht() {
	//Yang Hui triangle 杨辉三角
	//第0列和对角线上的元素均为1,其他元素的值均为上一行的同列元素和前一列元素之和;
	int a[7][7], i, j;
	for (i = 0; i < 7; i++)
	{
		a[i][0] = 1;//第0列
		a[i][i] = 1;//对角线
	}
	//从第三行第二列开始赋值,第几行就有几列
	for (i = 2; i < 7; i++)
	{
		for (j = 1; j < i; j++)
		{
			a[i][j] = a[i - 1][j] + a[i - 1][j - 1];
		}

	}
	//打印格式,第几行就有几列
	for (i = 0; i < 7; i++)
	{
		for (j = 0; j <= i; j++)
		{
			printf("%3d", a[i][j]);
		}
		printf("\n");
	}
}

void addnum(int n)
{
	printf("要插入的数是%d:",n);
	int i, option = 0;
	int a[11] = { 1,3,5,7,9,11,13,15,17,19 };	
	printf("原顺序:");
	for (i = 0; i < 10; i++)
		printf("%3d", a[i]);
	if (a[0] < a[1])
		//升序排序
		while (n > a[option])
			//找到应该插入的位置
			option++;
	else
		//降序排序
		while (n < a[option])
			//找到应该插入的位置
			option++;

	//找到option,进行插入
	for (i = 10; i > option; i--) {
		a[i] = a[i - 1];
	}
	a[i] = n;
	printf("\n插入之后的顺序:");
	for (i = 0; i < 11; i++)
	{
		printf("%3d", a[i]);

	}
	printf("\n");
}

void delnum(int option)
{
	int i;
	int a[10] = { 1,3,5,7,9,11,13,15,17,19 };	
	printf("要删除是:a[%d]=%d\n",option,a[option]);
	printf("原顺序:");
	for (i = 0; i < 10; i++)
	{
		printf("%3d", a[i]);
	}
	//找到option,进行删除
	for (i = option; i < 9; i++)
		a[i] = a[i + 1];
	a[9] = '\0';
	printf("\n现顺序:");
	for (i = 0; i < 9; i++)
	{
		printf("%3d", a[i]);

	}
	printf("\n");
}

void inv_row_colum() {
	int i, j;
	int a[3][4] = { 1,3,5,7,9,11,13,15,17,19,21,23 }, b[4][3];
	for (i = 0; i < 4; i++)
		for (j = 0; j < 3; j++)
			b[i][j] = a[j][i];
	printf("array a:\n");
	for (i = 0; i < 3; i++) {
		for (j = 0; j < 4; j++) {
			printf("%5d", a[i][j]);
		}
		printf("\n");
	}
	printf("array b:\n");
	for (i = 0; i < 4; i++) {
		for (j = 0; j < 3; j++) {
			printf("%5d", b[i][j]);
		}
		printf("\n");
	}
}

void printarray2() {
	//打印矩阵
	int j, i, a[5][5];
	int N = 5;
	for (i = 0; i < N; i++) {
		for (j = 0; j < N; j++) {
			if (i <= j)
				a[i][j] = 1;//对角线以上都为1
			else if (j == 0)
				a[i][j] = i + 1;//对角线一下的第一列递增
			else
				a[i][j] = a[i - 1][j - 1];//每一条斜线上的值都相等
		}
	}
	for (i = 0; i < N; i++) {
		for (j = 0; j < N; j++) {
			printf("%4d", a[i][j]);
		}
		printf("\n");
	}
}

void printa_b() {
	int j, i, k, a[3][4] = { 1,2,3,4,5,6,7,8,9,10,11,12 };
	int b[4][5] = { 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20}, c[3][5];
	printf("array A(3*4):\n");
	for (i = 0; i < 3; i++) {
		for (j = 0; j < 4; j++) {
			printf("%4d", a[i][j]);
		}
		printf("\n");
	}
	printf("array B(4*5):\n");
	for (i = 0; i < 4; i++) {
		for (j = 0; j < 5; j++) {
			printf("%4d", b[i][j]);
		}
		printf("\n");
	}
	for (i = 0; i < 3; i++) {
		for (j = 0; j < 5; j++) {
			c[i][j] = 0;
			for (k = 0; k < 4; k++)//k为a的列数,也为b的行数,只有当a的列数=为b的行数时,矩阵乘法才有意义
				c[i][j] += a[i][k] * b[k][j];//a的第i行与第j列分别相乘,再相加即为c[i][j];
		}
	}
	printf("array C(3*5)=A * B:\n");
	for (i = 0; i < 3; i++) {
		for (j = 0; j < 5; j++) {
			printf("%4d", c[i][j]);
		}
		printf("\n");
	}
}

void sort(int a[]) {
	//使用选择法,将数组中10各整数从小到大排序
	int i, j, k, t;
	for (i = 0; i < 9; i++)
	{
		k = i;
		for (j = i + 1; j < 10; j++)
		{
			//轮
			if (a[j] < a[k])
			{
				k = j;//记录最小值下标
			}
		}
		//将最小值进行对换
		t = a[k];
		a[k] = a[i];
		a[i] = t;
	}
}

void inv(int a[], int n) {
	//将一个数组中的n个数据按颠倒的顺序重新存放
	int t, i, j, m = (n - 1) / 2;
	for (i = 0; i <= m; i++)
	{
		j = n - 1 - i;
		t = a[i];
		a[i] = a[j];
		a[j] = t;

	}
}

void inv2(int a[], int n) {
	//将一个数组中的n个数据按颠倒的顺序重新存放
	int t, i, j;
	for (i = 0,j=n-1; i <j; i++,j--)
	{
		t = a[i];
		a[i] = a[j];
		a[j] = t;
	}
}


double inteq(double a, double b) {
	//利用梯形法求定积分
	//实际上就是求函数f(a+i*h)值的累加过程
	//a为积分上限,b为积分下限
	//i的值由1到n-1;
	double fd(double x);//声明
	int n = 100, i;//n为积分区间分割数,设n=100
	double h = fabs(a - b) / n;//高
	double s = (fd(a) + fd(b)) / 2.0;//累积和的第一项
	double x;
	for (i = 1; i <= n - 1; i++)
	{
		x = a + i * h;
		s += fd(x);
	}
	return s * h;
}

double fd(double x) {
	//定积分
	return sin(x);
}

void countch(char a[], int b[]) {
	//统计一行字符中,出现的每个小写字母的个数
	//a[]字符,b[]26个字母
	int i, n, option;
	n = strlen(a);
	for (i = 0; i < n; i++)
	{
		if (a[i] >= 'a'&&a[i] <= 'z')
		{
			option = a[i] - 97;//判断是第几个字母,从而作为下标
			b[option]++;//该位置的字母个数加1;
		}
	}

}
//第七章
float get_t_s(float a, float b, float c) {
	//Triangle,求三角形的面积
	//abc三条边
	float f = 0, s;//f为面积
	s = (a + b + c) / (float)2.0;
	if ((s - a) > 0 && (s - b) > 0 && (s - c) > 0)
	{
		f = (float)sqrt(s*(s - a)*(s - b)*(s - c));//平方根必须为正
	}
	return f;
}

float getseries5(int m) {
	//计算t=1-1/2*2-1/3*3-...-1/m*m
	int i;
	float t = 1.0;
	for (i = 2; i <= m; i++)
	{
		t = t - (float)1.0 / (i * i);
	}
	return t;
}

int reverse(int m) {
	//尾数非0的正整数的逆序数
	//先判断是几位数
	int x = 0;
	while (m)
	{
		x = x * 10 + m % 10;//循环加尾数
		m = m / 10;//循环去掉尾数
	}
	return x;
}

void getcount(char s[]) {
	int i, num = 0, ch = 0, sp = 0, oh = 0;
	char c;
	for (i = 0; (c = s[i]) != '\0';i ++)
		if (c == ' ') sp++;
		else if (c >= '0'&&c <= '9') num++;
		else if (toupper(c) >= 'A' && toupper(c) <='Z') ch++;
		else oh++;
	printf("char:%d,number:%d,space:%d,other:%d\n", ch, num, sp, oh);
}

//位运算
void printbin(short int num) {
	//转换二进制
	short int mask, i;
	//mask = 1 << 15;//最高位为1,其余为0,用于屏蔽字
	printf("%d=", num);
	for (i =1; i <=16; i++) {
		mask = 1 << (16-i);//次高为为1,其余为0,用于次高位,屏蔽字
		putchar(mask & num ? '1' : '0');//取1对应的值
		//num <<= 1;//将次高位移到最高位
		if (i % 4 == 0)putchar(',');//4位一组		
	}
	printf("\bB\n");//\b是退一格,并覆盖最后的符号
}

void printbin8_11(short int num) {
	//取8-11位
	short int mask;
	printf("the number:0x%x\n", num);
	num >>=8;//将8-11位转到低4位
	mask =~(1<<4);//将4位为1,其余为0,屏蔽字
	printf("8-11:0x%x\n", num & mask);
	
}

void printbinex(unsigned short a,int n) {
	//将a循环右移n个数据
	unsigned short  b,c;
	b = a << (16 - n);//把a的右端n位放到b的高n位
	c = a >> n;//把a右移n位,左边高位补0;
	c = c | b;//按位或
	printf("%u\n%u\n",a,c);

}
//指针

void dynamicarray() {
	//动态分配一段内存空间用于存储一个可变大小的整型数组
	//数组的大小,和元素由键盘输入
	int i,n, *p;
	printf("please input size of array:");
	scanf_s("%d",&n);
	p = (int *)calloc(n, sizeof(int));//分配n个大小为size的连续存储空间;p==>数组名
	printf("please input member of array(number=%d):",n);
	for ( i = 0; i < n; i++)
	{
		scanf_s("%d",&p[i]);
	}
	printf("dynamic array:");
	for (i = 0;i < n; i++)
	{
		printf("%d\t",p[i]);
	}
	printf("\n");
	free(p);//释放动态分配的内存空间
}

void printarray() {
	//一维数组与指针,指针变量与数组名之间的互换关系,访问第i个元素的几种方法
	//计算并输出一个数组中所有元素的和,最大值,最小值,值为奇数的元素的个数
	int a[10],i, *p,sum, max, min, count;
	for (i = 0; i < 10; i++)
		a[i] = i + 1;//1.利用数组下标赋值
	p = a;//使p指向数组a的首地址

	sum = 0;
	for (i = 0; i < 10; i++) {
		sum += *(p + i);//2.利用指针变量偏移量求和
	}
	max = *a;//利用数组首地址求值
	for (i = 0; i < 10; i++) {
		if (max < *(a + i)) {
			max = *(a+i);//3.利用数组首地址偏移量求最大值
		}
	}
	min = p[0];//利用指针下标
	for (i = 0; i < 10; i++,p++) {
		if (min > *p) {
			min = *p;//4.利用指针移动求最小值
		}
	}
	p = &a[0];//指针移动了之后,记得再次使p指向数组a的首地址
	count = 0;
	for (i = 0; i < 10; i++) {
		if (p[i] % 2!= 0) {
			count++;
		}
	}
	printf("sum=%d\tmax=%d\tmin=%d\tcount(even)=%d\n",sum,max,min,count);
}
void prnArray(int a[], int num) {
	int i;
	for (i = 0; i < num; i++)
	{
		printf("%d\t", a[i]);
	}
	printf("\n");
}

void swap(int *px ,int *py) {
	int t;
	t = *px;//t=x
	*px = *py;//x=y
	*py = t;//y=x

}

void invarray(int a[],int n) {
//用指针实现,将数组a中的n个整数按相反顺序排序
	int *i,*j;
	for (i = a, j =a+ n - 1; i < j; i++, j--)
		swap(i,j);
}
	
 void bubarray(int a[],int start, int end) {
	//实现将数组任意指定范围内的数据按从小到大
	int i,j;
	for (i = start; i < end; i++) {
		for ( j = start;j<end-i; j++)
		{
			if (a[j]>a[j+1])
			{
				int t = a[j];
				a[j] = a[j + 1];
				a[j + 1] = t;
			}
		}
	}
}

 void bubarray1(int a[], int n) {
	 int i, j;
	 for (i = 0; i < n - 1; i++)
	 {
		 for (j = 0; j < n - 1 - i; j++)
		 {
			 if (a[j]>a[j + 1])//升序排序
			 {
				 swap(&a[j], &a[j + 1]);
			 }
		 }

	 }
 }
 void bubarray2(int a[],int n) {
	 //用指针实现,将数组任意指定范围内的数据按从小到大
	 int *i, *j;
	 for (i = a + n - 1; i >a ; i--) {//i指向最后一个元素,外层一样的循环n-1次
		 for (j = a; j<i; j++)//在j和i之间的元素比较,第一次0-9,第二次0-8,所以还是口诀中的n-1-i轮
		 {
			 if (*j > *(j + 1))
			 {
				 swap(j,j+1);
			 }
		 }
	 }
 }
 void bubarray4(int a[], int n) {
	 int *i, *j;
	 for (i = a; i <a+n-1; i++)
	 {
		 printf("times %d:\n", n-(i-a));
		 for (j = a; j<a+n-1-(i-a); j++)
		 {
			 if (*j>*(j + 1))//升序排序
			 {
				 swap(j, j+1);
			 }
			 prnArray(a, n);
		 }

	 }
 }

 void cpystr(const char * s, char str[]) {
 //将一个字符串的内容复制到一个字符数组中
	 /*while (( *str = *s) != '\0') {//字符串以'\0'作为结束标记
		 *str = *s;
		 s++;//字符指针移向下一个
		 str++;//数组指针移向下一个
	 }*/


	 while (*str++ = *s++);//小心数组越界
 }

 int cmpstr(char * str1,char * str2) {
	 while (*str1&&*str1==*str2) {//遇到终止符号时退出,两个字符不相等时退出
		 str1++;
		 str2++;
	 }
	 return *str1 - *str2;
 }

 void yhtp() {
	 //Yang Hui triangle 杨辉三角,用指针实现
	 //第0列和对角线上的元素均为1,其他元素的值均为上一行的同列元素和前一列元素之和;
	 int a[7][7], i, j;
	 for (i = 0; i < 7; i++)
	 {
		*(*(a + i))= 1;// &a[i][0]==>*(a+i)+0//数组名作地址生成第0列
		*(a[i] + i) = 1;// &a[i][i]==>a[i]+i//行地址方式生成对角线
	 }
	 //从第三行第二列开始赋值,第几行就有几列
	 for (i = 2; i < 7; i++)
	 {
		 for (j = 1; j < i; j++)
		 {
			 a[i][j] = a[i - 1][j] + a[i - 1][j - 1];//下标方式
		 }

	 }
	 //打印格式,第几行就有几列
	 for (i = 0; i < 7; i++)
	 {
		 for (j = 0; j <= i; j++)
		 {
			 printf("%3d",*(a[i] + j));// &a[i][j]==>a[i]+j==>*(a+i)+j
		 }
		 printf("\n");
	 }
 }


 void yhtp2() {
	 int a[7][7] = {1}, i, j, *p;
	 a[1][0] = 1;
	 //从第1行的第1个元素开始赋值
	 for (p =&a[1][1];p<=&a[6][6];p++)
	 {
		 //a[i][j]就是顺序排序中第i*N+j个元素地址为p,值等于第i*N+j-N(即p-N)和第i*N+j-N-1(即p-N-1)的值的和
		 *p = *(p - 7) + *(p - 7 - 1);
	 }
	 for (i = 0; i < 7; i++)
	 {
		 p = &a[i][0];
		 for ( j = 0; j <=i; j++,p++)
		 {
			 printf("%3d",*p);
		 }
		 printf("\n");
	 }
 }
 //行指针
 void printtwo() {
 //应用指针数组输出二维数组中各元素
	 int a[4][3]{0,1,2,3,4,5,6,7,8,9,10,11};
	 int * pa[4], i, j;
	 for ( i = 0; i < 4; i++)
	 {
		 pa[i] = a[i];//各行首地址
	 }
	 for (i = 0; i < 4; i++)
	 {
		 for (j = 0; j < 3;j++)
		 {
			 printf("%3d",pa[i][j]);//引用
		 }
		 printf("\n");
	 }
 }
 //指针数组
 void dirstr(const char * name[],int n) {
	 //将字符串按字典顺序从小到大排列后输出
	 //name即需要排序的字符串,n为字符串个数
	 int i, j, k;
	 for (i = 0; i < n-1; i++) {
		 k=i;
		 for ( j = i+1; j < n; j++)
		 {
			 if (strcmp(name[k],name[j])>0)
			 {
				 k = j;
			 }
		 }
		 if (k!=i)
		 {
			const char *pt = name[i];
			 name[i] = name[k];
			 name[k] = pt;
		 }
	 }
	 


 }

 //指针练习
 void compute(int m,int n,int*sum,int*p) {
 //计算m,n的和;m,n的积,并通过参数返回
	 *sum = m + n;
	 *p = m * n;
 }

 void subnum(char  s[81]) {
	 //对字符串提取数字字符
	 char c[80];
	 int i = 0;
	 char * str=s;
	 while (*str)
	 {
		 if (*str>='0'&&*str<='9')
		 {
			 c[i] =*str;
			i++;
		 }	
		 str++;
	 }
	 c[i] = '\0';
	 strcpy_s(s, strlen(c)+1, c);
 }

 void sortpeople(int n,int start,int a,int b) {
	//有N个人围一圈,顺序排号
	 //由位置S开始报数,计数从A开始,凡报为B的倍数出圈
	 //问最后剩下的是几号?
	 int i = 0, call_i, out_i, *p;
	 p = (int *)calloc(n, sizeof(int));//动态分配n个大小为size的连续存储空间==>由n个人组成的数组
	 for (; i < n; i++) {
		 *(p + i) = i + 1;//给n个人顺序排号
	 }
	 i = start;//起报位置,下标
	 call_i = a-1;//计数变量准备
	 out_i = 0;//出局人数  
	 while (out_i<n-1)//循环报号,直到剩一个人
	 {
		 if (*(p + i) != 0) 
			 call_i++;//报号		 
		 if (call_i == b) {//报到b的就out出局
			 out_i++;
			 printf("第%d个出局的是:%d\n", out_i,p[i]);			
			 *(p + i) = 0;//出局的置0
			 call_i =a -1;//报号重新开始
		 }
		 i++;//让指针移动
		 if (i == n) 
			 i = 0;//已经从头到尾报了一遍,开始下一次循环		
	 }
	 while (*p==0)
	 {
		 p++;
	 }
	 printf("最后剩下的是:%d\n",*p);
	 free(p);//释放空间
 }
	  
 //位段
struct bs//struct 位段结构名
{
	//位段列表
	//类型说明符 位段名:位段长度;
	unsigned a : 8;//位段类型必须是unsigned类型,位段可以采用%d,%u,%o,%x等格式输出
	unsigned b : 2;//位段没有地址,因此不能进行取地址运算
	unsigned c : 6;
	unsigned : 4;//可以定义无名位段,只是用来填充/调整位置
	unsigned : 0;//可以定义长度为0的位段方式,使下一个位段从下一个单元开始;
	unsigned d : 23;//一个位段必须存储在同一个存储单元中,不能跨两个单元.所以长度<=存储单元长度
	unsigned e : 24;//需要从下一单元开始存放
	unsigned i;//成员i,从下一个单元开始存放
};

//结构体的定义,可以在函数的内部,也可以定义在函数的外部
struct student//struct 结构体名
{
	char num[10];//类型名1 成员名1;
	char name[20];//类型名2 成员名2;
	char sex;
	int age;
	float score;
};//必须要分号


