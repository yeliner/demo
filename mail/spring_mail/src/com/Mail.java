package com;

import java.util.Date;

import org.springframework.mail.MailSender;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;

public class Mail {
	private JavaMailSender mailSender;


	public JavaMailSender getMailSender() {
		return mailSender;
	}


	public void setMailSender(JavaMailSender mailSender) {
		this.mailSender = mailSender;
	}


	public void send(){
		SimpleMailMessage mail=new SimpleMailMessage();
		mail.setFrom("1414365522@qq.com");
		mail.setTo("1571358421@qq.com");
		mail.setSubject("testSpringMail");
		mail.setText("spring_JavaMail����");
		mail.setSentDate(new Date());
		mailSender.send(mail);
	}

}
