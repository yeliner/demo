package task4;

// 此处定义animal类
abstract class Animal {
    //animal为抽象类
    abstract void move();//抽象方法,没有方法体
}

//  此处定义action接口
interface Anction {
    //action接口有一个sing()方法
    void sing();
}

//  此处定义bird类
class Bird extends Animal implements Anction {
    //继承抽象类,必须重写覆盖其父类的抽象方法
    @Override
    void move() {
        System.out.println("I`m a bird,I`m flying");
    }

    //实现接口,必须重写覆盖其所有方法
    @Override
    public void sing() {
        System.out.println( "Sing a song");
    }
}

//  此处定义beast类
class Beast extends Animal {

    @Override
    void move() {
        System.out.println("I`m a beast,I`m running");
    }
}

//  此处定义fish类
class Fish extends Animal {
    @Override
    void move() {

        System.out.println("I`m a fish,I`m swimming");
    }
}

public class Abstract_Show {
    public static void main(String[] args) {
        Animal anml[] = new Animal[3];
        anml[0] = new Bird();
        anml[1] = new Beast();
        anml[2] = new Fish();
        for (int i = 0; i < anml.length; i++) {
            System.out.print(anml[i].getClass().getName() + " say: ");
            anml[i].move();
        }
        ((Bird) anml[0]).sing();
    }
}
