package task4;

public class Rectangle extends Shape {
    //矩形
    private double a, b;

    public Rectangle() {
    }

    public Rectangle(double a, double b) {
        this.a = a;
        this.b = b;
    }

    @Override
    double getCir() {
        return 2 * (a + b);
    }

    @Override
    double getS() {
        return a * b;
    }

    public double getA() {
        return a;
    }

    public double getB() {
        return b;
    }

    public void setA(double a) {
        this.a = a;
    }

    public void setB(double b) {
        this.b = b;
    }
}
