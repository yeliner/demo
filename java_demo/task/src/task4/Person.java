package task4;

class Person {
    public String name;
    public String gender;
    public int age;

    public Person() {
    }

    public Person(String name, String gender, int age) {
        this.name = name;
        this.gender = gender;
        this.age = age;
    }

    public String toString() {
        return "姓名：" + name + "，性别" + gender + "，年龄" + age;
    }
}

//  此处补充缺失的Student类代码
/*（1） Person类中的name, gender, age等属性和toString()方法能否被修饰为 private 或者protected？
*不能,因为后面调用是直接调用
* */

class Student {
    String sno = "13001";   //  学号
    String name = "李波";   //  姓名
    String gender = "男";   //  性別
    int age = 21;   //  年齡
    String major = "化学";   //  专业
    String tel = "1380***1236";   //  电话

    Student() {
    }

    Student(String name, String gender, int age) {
        this.name = name;
        this.gender = gender;
        this.age = age;
    }

    Student(String name, String gender, int age, String sno, String major, String tel) {
        this.name = name;
        this.gender = gender;
        this.age = age;
        this.sno = sno;
        this.major = major;
        this.tel = tel;
    }

    public String toString() {
        String s = "姓名:" + name + "\t性別:" + gender + "\t年齡:" + age + "\t学号:" + sno + "\t专业:" + major + "\t电话:" + tel;
        return s;
    }

}

