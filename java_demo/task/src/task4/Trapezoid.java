package task4;

public class Trapezoid extends Shape {
    //梯形
    private double top, bot, c, d, h;//上底,下底,左右,高

    public Trapezoid() {
    }

    public Trapezoid(double top, double bot, double c, double d, double h) {
        this.top = top;
        this.bot = bot;
        this.c = c;
        this.d = d;
        this.h = h;
    }

    @Override
    double getCir() {
        return top+bot+c+d;
    }

    @Override
    double getS() {
        return (top+bot)*h;
    }

    public double getTop() {
        return top;
    }

    public void setTop(double top) {
        this.top = top;
    }

    public double getBot() {
        return bot;
    }

    public void setBot(double bot) {
        this.bot = bot;
    }

    public double getC() {
        return c;
    }

    public void setC(double c) {
        this.c = c;
    }

    public double getD() {
        return d;
    }

    public void setD(double d) {
        this.d = d;
    }

    public double getH() {
        return h;
    }

    public void setH(double h) {
        this.h = h;
    }
}
