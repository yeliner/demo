package task4;

public class Teacher extends Person {
    private String dName;
    private String dept;

    public Teacher() {
        super();
    }

    public Teacher(String name, String gender, int age, String dName, String dept) {
        super(name, gender, age);
        this.dName = dName;
        this.dept = dept;
    }

    public String toString() {
        return super.toString() + ",职称:" + dName + ",部门:" + dept;
    }

    public String getdName() {
        return dName;
    }

    public String getDept() {
        return dept;
    }

    public void setdName(String dName) {
        this.dName = dName;
    }

    public void setDept(String dept) {
        this.dept = dept;
    }
}
