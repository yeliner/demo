package task4;

public class Cricle extends Shape {
    private double r;

    public Cricle() {//构造方法
    }

    public Cricle(double r) {//有参构造方法
        this.r = r;
    }

    @Override
    double getCir() {
        //获取圆周长
        return 2 * Math.PI * r;
    }
    @Override
    double getS() {
        return Math.PI * r * r;
    }

    public void setR(double r) {
        this.r = r;
    }

    public double getR() {
        return r;
    }

}
