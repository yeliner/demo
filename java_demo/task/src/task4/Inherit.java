package task4;

public class Inherit {
    public static void main(String[] args) {
        Student stu1 = new Student();
        Student stu2 = new Student("王刚", "男", 21);
        Student stu3 = new Student("刘丽", "女", 20, "13003", "英语", "1380***1234");

        stu1.name = "李波";   //  学号
        stu1.gender = "男";   //  专业
        stu1.age = 21;   //  电话
        stu1.sno = "13001";   //  学号
        stu1.major = "化学";   //  专业
        stu1.tel = "1380***1236";   //  电话
        stu2.sno = "13002";   //  学号
        stu2.major = "物理";   //  专业
        stu2.tel = "1380***1235";   //  电话
        System.out.println("stu1的信息：" + stu1.toString());
        System.out.println("stu2的信息：" + stu2.toString());
        System.out.println("stu3的信息：" + stu3.toString());
    }
}
