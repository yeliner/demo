package task4;

/**
 * 3. 定义一个代表“形状”的抽象类，其中包括求形状面积和周长的抽象方法。
 * 继承该抽象类定义三角型、矩形、圆、梯形。
 * 分别创建一个三角形、矩形、圆、梯形存入一个数组中，访问数组元素将各类图形的面积输出。
 */
public abstract class Shape {
    abstract double getCir();//获取周长
    abstract double getS();//面积


}
