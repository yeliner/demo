package task1;

import java.util.Scanner;

/**
 * 1. 编写程序从命令行输入半径，求圆周长及圆面积。
 */
public class TestCricle {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        //测试圆
        System.out.println("你好,请输入圆半径r:");
        double r = input.nextDouble();
        Cricle cricle = new Cricle(r);//类对象
        System.out.println("圆半径为:" + cricle.getR() + "\t周长:" + cricle.getCir() + "\t面积" + cricle.getS());
    }
}
