package task1;

/**
 * 定义圆,属性为半径,提供求半径和面积的方法
 */
public class Cricle {
    private double r;

    public Cricle() {//构造方法
    }

    public Cricle(double r) {//有参构造方法
        this.r = r;
    }


    public double getCir() {
        return 2 * Math.PI * r;
    }//获取圆周长

    public double getS() {
        return Math.PI * r * r;
    }//获取圆面积

    public void setR(double r) {
        this.r = r;
    }

    public double getR() {
        return r;
    }
}
