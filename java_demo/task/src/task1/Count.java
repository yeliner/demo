package task1;

import java.util.Scanner;

/**
 * 5.输入一个长整数，统计该数的数字系列中大于等于4的数字个数。
 */
public class Count {
    public static void main(String[] args) {

        Scanner input = new Scanner(System.in);
        System.out.println("你好,请输入一个长整数:");
        long num = input.nextLong();//长整数用long接收
        int count = 0;//个数
        //不确定次数,用while循环
        while (num > 1) {
            if (num % 10 >= 4)//获取最后一位
                count++;
            num = num / 10;//循环,去掉最后一位
        }
        System.out.println("该数的数字系列中大于等于4的数字个数为:" + count);

    }
}
