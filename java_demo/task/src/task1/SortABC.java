package task1;

import java.util.Scanner;

/**
 * 2. 从键盘输入三个整数，按有小到大的顺序输出。
 */
public class SortABC {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);//接收输入数据
        System.out.println("你好,请输入第一个整型数:");
        int num1 = input.nextInt();
        System.out.println("你好,请输入第二个整型数:");
        int num2 = input.nextInt();
        System.out.println("你好,请输入第三个整型数:");
        int num3 = input.nextInt();
        System.out.println("before:\t" + num1 + "\t" + num2 + "\t" + num3);
        //因为此处是3个数字,所以我打算直接求最大,最小,中间值
        int max = Math.max(Math.max(num1, num2), num3);//最大的数
        int min = Math.min(Math.min(num1, num2), num3);//最小数
        int mid = max == num1 ?
                (min == num2 ? num3 : num2) :
                (max == num2 ? (min == num1 ? num3 : num1) : (min == num2 ? num1 : num2));
        System.out.println("after dec:\t" + min + "\t" + mid + "\t" + max);

    }
}
