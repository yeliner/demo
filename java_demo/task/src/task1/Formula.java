package task1;

import java.util.Scanner;

/**
 * 4. 从键盘输入一元二次方程的三个系数，计算方程ax^2+bx+c=0的根。
 */

public class Formula {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        float a, b, c, d, x1, x2;//x1,x2即为根
        System.out.println("请输入一元二次方程的三个系数a b c\n");
        a = input.nextInt();
        b = input.nextInt();
        c = input.nextInt();
        //(1)先判断是几元几次方程
        if (a == 0) {
            if (b == 0) {
                if (c == 0) {
                    System.out.println("该方程有任意解\n");//方程式:0=0
                } else {
                    System.out.println("该方程无解\n");//方程式:c=0
                }
            } else {
                System.out.println("该方程是一元一次方程，有一个解是:" + (-c / b));//bx+c=0;
            }
        } else {
            //为一元二次方程ax^2+bx+c=0(a<>0)
            //一元二次方程有且仅有两个根，根的情况由判别式（ b*b-4*a*c ）决定
            //(2)判断判别式的值
            d = b * b - 4 * a * c;
            if (d < 0)
                System.out.println("该方程无实数根\n");//方程无实数根，但有2个共轭复根。
            else if (d == 0) {
                System.out.println("该方程有2个相等的实根是x1=x2=" + (-b / 2 / a));//方程有两个相等的实数根；
            } else {
                System.out.println("该方程有2个不等实根，分别是\n");//该方程有2个不等实根
                x1 = (float) (-b + Math.sqrt(d) / 2 / a);
                x2 = (float) (-b - Math.sqrt(d)) / 2 / a;
                System.out.println("x1=" + x1 + "\tx2=" + x2);
            }
        }
    }
}
