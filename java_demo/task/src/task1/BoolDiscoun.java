package task1;

import java.util.Scanner;
 /**
  *3. 编写Java应用程序，实现运输公司对用户计算运输费用。
  * */
public class BoolDiscoun {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("你好,请输入路程s:");
        double s = input.nextDouble();//接收
        //分段,连续空间,用if-else分支
        if (s >= 3000) {
            System.out.println("15％折扣");
        } else if (s >= 2000) {
            System.out.println("10％折扣");
        } else if (s >= 1000) {
            System.out.println("8％折扣");
        } else if (s >= 500) {
            System.out.println("5％折扣");
        } else if (s >= 250) {
            System.out.println("2％折扣");
        } else {
            System.out.println("没有折扣");
        }
    }
}
