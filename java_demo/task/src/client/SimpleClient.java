package client;

import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.Socket;

public class SimpleClient {
    public static void main(String [] args){
        //Socket(主机,port)
        Socket s1;

        try {
            s1=new Socket("localhost",5432);//请求链接
            InputStream is=s1.getInputStream();//输入
            DataInputStream dis=new DataInputStream(is);
            String info=new String(dis.readUTF());//读取
            System.out.println(info);
            s1.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
