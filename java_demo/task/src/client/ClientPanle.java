package client;

import javax.swing.*;
import java.awt.*;

public class ClientPanle extends JFrame {

    public ClientPanle() {
        JTextArea display = new JTextArea(20, 80);
        JTextField input = new JTextField(20);
        JPanel p = new JPanel();
        p.add(display,BorderLayout.CENTER);
        p.add(input,BorderLayout.SOUTH);
        add(p);

    }

    public static void main(String[] args) {
        ClientPanle frame = new ClientPanle();
        frame.setTitle("客户端");
        frame.setSize(700, 450);
        frame.setLocationRelativeTo(null);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);
    }
}
