package task2;

/**
 * 1. 计算n至少多大时，一下不等式成立：1+1/2+1/3+...+1/n>6
 */

public class Fraction {
    public static void main(String[] args) {
        //规律:分子不变,分母依次加1,利用循环
        //分式计算,注意精度,用float
        float s = 0, t = 0;//s为和,t为项
        int i = 0;
        // 因为不知道次数,所以用while循环
        while (s <= 6) {
            i++;
            t = (float) 1.0 / i;
            s = s + t;

        }
        System.out.println("n的值为" + i+"\n"+"s的值为"+s);
}
}

