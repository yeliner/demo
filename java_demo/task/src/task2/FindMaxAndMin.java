package task2;

import java.util.Arrays;
import java.util.Scanner;

/**
 * 2. 编写一个程序从键盘输入10个数，将最大、最小的整数找出来并输出。
 */
public class FindMaxAndMin {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int[] nums = new int[10];//10个数存放在数组
        System.out.println("请输入10个数(空格隔开,超出个数不算入)");
        //确定次数用for循环
        for (int i = 0; i < 10; i++) {
            nums[i] = input.nextInt();
        }
        System.out.println("输入的10个数为:");
        for (int item : nums
                ) {
            System.out.print(item + "\t");
        }
        System.out.println();
        Arrays.sort(nums);
        System.out.println("最小数为:" + nums[0] + "\t最大数为:" + nums[9]);
    }
}
