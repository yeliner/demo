package task2;

import java.util.HashSet;
import java.util.Scanner;

/**
 * 3. 幻方是一个奇数阶矩阵，每个元素值不同，
 * 且它的每一行之和、每一列之和、左对角线之和以及右对角线之和都等于一个相同的数。
 * 编写一个程序验证输入的3阶矩阵是否为幻方。一下为两组验证数据。
 * 4	9	2                 49	 113	17
 * 3	5	7                 29    59     89
 * 8	1	6                 101   5      71
 */
public class Matrices {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int[][] nums = new int[3][3];//存放在二维数组
        System.out.println("请输入矩阵(3*3):");
        //确定次数用for循环
        for (int i = 0; i < 3; i++) {//二维,双重循环
            for (int j = 0; j < 3; j++) {
                nums[i][j] = input.nextInt();
            }
        }
        boolean flag = true;//先默认是幻方
        //判断元素值是否不等
        HashSet<Integer> hashSet = new HashSet<Integer>();//hashSet不包含重复元素的集合。
        for (int i = 0; i < 3; i++) {//二维,双重循环
            for (int j = 0; j < 3; j++) {
                hashSet.add(nums[i][j]);//把数组元素存入hashset,并且不存入多余重复的值
            }
        }
        if (flag=(hashSet.size()== 9)) {
            //先把对角线和求出
            int sum1 = nums[0][0] + nums[1][1] + nums[2][2];
            int sum2 = nums[0][2] + nums[1][1] + nums[2][0];
            if (sum1 != sum2) {//先判断对角线和是否相等
                flag = false;
            } else {
                int s = sum1;//如果相等,保存和值
                for (int i = 0; i < 3; i++) {
                    sum1 = 0;
                    sum2 = 0;
                    for (int j = 0; j < 3; j++) {
                        sum1 += nums[i][j];//计算每一行之和
                        sum2 += nums[j][i];//计算每一列之和
                    }
                    if (s != sum1 || s != sum2) {//判断行,列,对角线是否相等
                        flag = false;
                        break;
                    }
                }
            }
        }
        System.out.println("是否为幻方:" + flag);
    }
}