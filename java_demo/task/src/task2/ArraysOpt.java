package task2;

import java.util.Scanner;

/**
 * 5. 编写并验证如下方法：
 * （1）对一维数组进行从大到小排序。
 * （2）求一维数组所有元素的平均值。
 * (3）查找某个数据在数组中的出现位置。
 */


public class ArraysOpt {
    public static void main(String[] args) {
        int[] nums = {77, 6, 8, 9, 6, 5, 4, 8, 3};//一维数组
        System.out.println("arrays:");
        for (int item : nums
                ) {
            System.out.print(item + "\t");
        }
        //(1)从大到小排序。
        sortByDesc(nums.clone());//不影响原本数组的排序
        //(2)求一维数组所有元素的平均值。
        System.out.println("\navg:"+getAvg(nums));
        //(3)查找某个数据在数组中出现的位置
        System.out.println("\n请输入需要查找的数");
        Scanner input = new Scanner(System.in);
        int n = input.nextInt();
        findIndex(nums, n);


    }

    private static float getAvg(int[] nums) {
        int sum=0;
        for (int num : nums
                ) {  sum+=num;
        }
        return (float)sum/nums.length;
    }

    private static void findIndex(int[] nums, int n) {
        //查找某个数据在数组中出现的位置
        int index = -1;//先假设该数据不存在
        for (int k = 0; k < nums.length; k++) {
            if (nums[k] == n) {
                index = k;//若存在,则下标
                break;
            }
        }
        if (index != -1) {
            System.out.print(n + "所在下标为" + index);
        } else
            System.out.print("该数据不存在");
    }

    private static void sortByDesc(int[] nums) {
        //利用冒泡排序
        for (int i = 0; i < nums.length - 1; i++) {
            for (int j = 0; j < nums.length - 1 - i; j++) {
                if (nums[j] > nums[j + 1]) {
                    int temp = nums[j];
                    nums[j] = nums[j + 1];
                    nums[j + 1] = temp;
                }
            }
        }
        System.out.println("\nBy Desc:");
        for (int item : nums
                ) {
            System.out.print(item + "\t");
        }
    }
}
