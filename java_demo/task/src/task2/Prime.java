package task2;

/**
 * 4. 写一个方法判断一个数是否为素数，返回布尔值。
 * 利用该方法验证哥德巴赫 猜想：任意一个不小于3的偶数可以拆成两素数之和。不妨将验证范围缩小到3~100。
 */
public class Prime {
    public static void main(String[] args) {
        for (int k = 3; k < 100; k++) {
            //验证哥德巴赫 猜想
            if (k % 2 == 0) {
                for (int i = 2; i < (k / 2)+1; i++) {
                    if (isPrime(i)) {
                        if (isPrime(k - i)) {//找i和num-i都为素数
                            System.out.println(k + "可以拆分为素数" + i + "与" + (k - i) + ",哥德巴赫猜想成立");
                        }
                    }
                }
            }
        }
    }

    private static boolean isPrime(int n) {
        //判断素数,素数只能被1和本身整除
        //2是最小的素数
        if (n == 1) {
            return false;//1不是素数
        }
        if (n == 2) {
            return true;
        } else {
            int i = 2;
            //素数肯定为奇数,或者i<=sqrt(n);
            while (i <= n / 2) {
                if (n % i == 0) {
                    return false;
                }
                i = i + 1;
            }
            return true;
        }
    }
}
