package task6;

import java.io.*;

public class FileOperate {
    public static void main(String args[]) {
        // 将诗歌写入文件 poem.txt
        try {
            // 实例化一个面向字符的 FileWriter 对象 fw ，指向项目根目录下的Demo文件夹下的文件 poem.txt
            FileWriter fw = new FileWriter("poem.txt");
            // 分4次写入静夜思的四行。在windows向文件中输出换行符用\r\n，据说在Linux下用\n
            fw.write("窗前明月光\r\n");
            fw.write("疑是地上霜\r\n");
            fw.write("举头望明月\r\n");
            fw.write("低头思故乡\r\n");
            fw.close();
        } catch (IOException e) {
            System.out.println("Write Something wrong!");
        }

        // 读出文件poem.txt的内容
        try {
            // 实例化一个 面向字符的 FileReader 对象 file ，指向项目根目录下的Demo文件夹下的文件 poem.txt
            FileReader file = new FileReader("poem.txt");
            // 实例化一个 LineNumberReader 对象 in ，指向 file。该对象能以行为单位读取数据
            LineNumberReader in = new LineNumberReader(file);
            boolean eof = false;
            while (!eof) {
                //从输入流 in 读1行文本赋给 String 变量 x
                String x = in.readLine();
                if (x == null)  // 判断是否读至文件尾
                    eof = true;
                else
                    System.out.println(x);
            }
            // 关闭文件流in
            in.close();
        } catch (IOException e) {
            System.out.println("Read Something wrong");
        }
        // 查看文件poem.txt的信息
        try {
            //  建立File类的实例 f ，指向Demo文件加下的 poem.txt
            File f = new File("poem");
            //  显示文件路径
            System.out.println("AbsolutePath:"+f.getAbsolutePath());
            System.out.println("Path:"+f.getPath());
            //  显示文件长度
            System.out.println("file is"+f.length()+"bytes");
            //  显示文件最后访问时间
            System.out.println("file lasttime:"+f.lastModified());
        } catch (Exception e) {
            System.out.println("info Somthing wrong!");
        }
    }
}

