package task5;

        import java.io.BufferedReader;
        import java.io.IOException;
        import java.io.InputStreamReader;

public class Exception_Show {
    public static void main(String args[]) {
        int fenmu;
        String s = "";
        System.out.print("请输入一个整数: ");
        try {
            BufferedReader in = new BufferedReader(new InputStreamReader(
                    System.in));
            s = in.readLine(); // 读一行数字型字符
        } catch (IOException e) {
        }
        try {
            fenmu = Integer.parseInt(s);//转换可能出错
            System.out.printf("100除以%d的值为：%d\n", fenmu, 100 / fenmu);//除数不能为0
        } catch (NumberFormatException e) {
            //捕捉格式错误
            System.out.println("erro:" + e.getMessage() + "\n输入不合法(必须为整数),请重新输入!");
        } catch (ArithmeticException e) {
            //捕捉算术错误,如除数不能为0
            System.out.println("erro:" + e.getMessage() + "\n除数不能为0!请重新输入!");
        }
        int xuhao;
        String s2 = "";
        System.out.print("现有一个含6个数据的数组，请输入一个序号: ");
        try {
            BufferedReader in = new BufferedReader(new InputStreamReader(
                    System.in));
            s2 = in.readLine(); // 读一行数字型字符，表示某数组的一个位置
        } catch (IOException e) {
        }
        int arr[] = {10, 20, 30, 40, 50, 60};
        try {
            xuhao = Integer.parseInt(s2);//转换错误
            System.out.println("您选择的数组元素为： " + arr[xuhao]);
        } catch (NumberFormatException e) {
            //捕捉格式错误
            System.out.println("erro:" + e.getMessage() + "\n转换错误,请输入一个整数!");
        }catch (ArrayIndexOutOfBoundsException e){
            //捕捉数组下标越界错误
            System.out.println("erro:" + e.getMessage() + "\n数组小标越界!请重新输入序号!");
        }

    }
}
