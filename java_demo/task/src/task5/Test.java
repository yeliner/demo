package task5;

public class Test {
    public static void main(String args[]) {
        String s1 = "helloworld";
        String s2 = "helloworld";
        boolean b1 = (s1.equals(s2));
        // 比较两个字符串是否相等
        System.out.println("s1是否等于s2？  " + b1);
        String s3 = "abcdefghijk";
        String s4 = s3.substring(3, 7);                 // 得到s3的第4个字符到第8个字符之间的子串
        String s5 = s3.replace("fgh", "QQ");            // 将s3中的子串"fgh"替换成"QQ"
        String s6 = s3.toUpperCase();                // 将s3中的所有字符换成大写字母
        char c = s3.charAt(6);              // 得到s3第6个位置的字符
        int pos = s3.indexOf('h');           // s3中"hij"出现的位置
        System.out.println("s3的第4个字符到第8个字符之间的子串= " + s4);
        System.out.println("s3中的子串\"fgh\"替换成\"QQ\"= " + s5);
        System.out.println("s3的所有字符换成大写字母= " + s6);
        System.out.println("s3的第7个字符= " + c);
        System.out.println("s3中\"hij\"出现的位置= " + pos);

        String s11 = "123";
        int n = Integer.parseInt(s11);              // 得到s11对应的整数
        String s12 = "123.06";
        float f = Float.parseFloat(s12);        // 得到s12对应的小数
        int m = 2012;
        String s13 = String.valueOf(m);            // 将m转换为字符串
        String s14 = s13 + 136;         //  将年份和数字136拼接成学号
        System.out.println("s11对应的整数= " + n);
        System.out.println("s12对应的整数= " + f);
        System.out.println("小明的学号是：" + s14);
        StringBuffer sbf = new StringBuffer("hello ");   // 创建一个StringBuffer对象sbf
        // 在sbf的第7个位置插入"beautiful "
        sbf.insert(6, "beautiful  ");
        // 向sbf中追加"may!"
        sbf.append("may");
        System.out.println(sbf);
    }
}

