package task3;

public class Ex1 {
    static int m = 2;

    public static void main(String args[]) {
        Ex1 obj1 = new Ex1();
        Ex1 obj2 = new Ex1();
        obj1.m = m + 1;
        System.out.println("m=" + obj2.m);
    }
    //1.程序运行结果:m=3
    //2.如果去掉变量m前的static修饰:加1操作会报错,因为在static方法中只能处理类变量
  //修改:obj1.m=obj1.m+1;访问的是obj1的类变量,就不会影响到obj2.m
}
