package task3;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Person {
    //  此处定义Person类的姓名、性别和年龄等成员
    private String name;
    private String gender;
    private int age;
    private Person[] children;

    public Person(String name, String gender, int age)   //  构造方法
    {
        //   此处补充代码，将姓名、性别和年龄分别赋值为name,gender, age
        this.name = name;
        this.gender = gender;
        this.age = age;

    }


    //  此处定义构造方法以及获取姓名、获取、性别、获取年龄、获取孩子数组等方法
    public Person() {
    }

    public String getName() {
        return name;
    }


    public String getGender() {
        return gender;
    }


    public int getAge() {
        return age;
    }

    public void addAge(int age) {
        this.age = age+1;
    }
    public Person[] getChildren() {
        return children;
    }

    public static void main(String[] args) {
        Person somebody = new Person("Tom", "male", 41);
        Person[] chs = somebody.getChildren();    // 获得子女数组名
        String cn = "";
        String sg = "";
        int sa = 0;
        int n = 0;  // 变量n用以记录孩子数量
        System.out.println("Please input the number of the children:");
        try {
            BufferedReader in = new BufferedReader(new InputStreamReader(
                    System.in));
            n = Integer.parseInt(in.readLine()); // 输入孩子数量
        } catch (IOException e) {
        }
        chs = new Person[n];    // 为子女数组名分配空间
        for (int i = 0; i < n; i++)  // 为n个孩子输入姓名、性别、年龄
        {
            System.out.println("Please input the name, gender and age of the child:");
            try {
                BufferedReader in = new BufferedReader(new InputStreamReader(
                        System.in));
                cn = in.readLine(); // 输入名字
                sg = in.readLine(); // 输入性别
                sa = Integer.parseInt(in.readLine()); // 输入年龄
            } catch (IOException e) {
            }
            chs[i] = new Person(cn, sg, sa);
        }

        //  输出somebody和他孩子们的姓名、性别、年龄
        System.out.println("somebody information: " + somebody.getName()
                + ',' + somebody.getGender() + ',' + somebody.getAge());
        for (int i = 0; i < n; i++) {
            System.out.println("The " + String.valueOf(i) + "th child is: " +
                    chs[i].getName() + ',' + chs[i].getGender() + ',' + chs[i].getAge());
        }
    }


}

