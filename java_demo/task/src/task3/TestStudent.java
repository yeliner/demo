package task3;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Scanner;

public class TestStudent {
    //编写一个学生类Student，包含的属性有学号、姓名、年龄，将所有学生存储在一个数组中，自拟数据，用数组的初始化方法给数组赋值。
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("Please input the number of the Student:");
        int n = input.nextInt();
        Student[] students = new Student[n];
        for (int i = 0; i < n; i++)
        {
            Student student = new Student();
            System.out.println("Please input the sno name age of the Student:");
            try {
                BufferedReader in = new BufferedReader(new InputStreamReader(
                        System.in));
                student.setSno(in.readLine());
                student.setName(in.readLine());
                student.setAge(Integer.parseInt(in.readLine()));
            } catch (IOException e) {
                e.printStackTrace();
            }
            students[i] = student;
        }
        //（1）将所有学生的年龄增加一岁。
        for (int i = 0; i < n; i++)
        {
            students[i].setAge(students[i].getAge() + 1);
        }
        System.out.println("学生名单:");
        //(2）按数组中顺序显示所有学生信息
        for (Student student : students) {
            System.out.println(student.getSno() + "\t" + student.getName() + "\t" + student.getAge());
        }
        //（3）查找显示所有年龄大于20岁的学生名单。
        System.out.println("年龄大于20岁的学生:");
        for (Student student : students) {

            if (student.getAge() > 20) {
                System.out.println(student.getSno() + "\t" + student.getName() + "\t" + student.getAge());
            }
        }
    }
}
