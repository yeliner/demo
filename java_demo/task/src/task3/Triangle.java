package task3;
/**
 * 编写一个代表三角形的类。
 * 其中三条边为三角形的属性，并封装有求三角形的面积和周长的方法。
 * 分别针对三条边为3,4,5和7,8,9的两个三角形进行测试。
 */
public class Triangle {

    private double a, b, c;

    public Triangle() {
    }

    public Triangle(double a, double b, double c) {
        this.a = a;
        this.b = b;
        this.c = c;
    }

    public double getS() {
        //（海伦公式）（p=(a+b+c)/2）,S=sqrt[p(p-a)(p-b)(p-c)]
        double p = (a + b + c) / 2;
        return Math.sqrt(p*(p - a)*(p - b)*(p - c));
    }

    public double getCC() {
        return a+b+c;
    }

    public double getA() {
        return a;
    }

    public void setA(double a) {
        this.a = a;
    }

    public double getB() {
        return b;
    }

    public void setB(double b) {
        this.b = b;
    }

    public double getC() {
        return c;
    }

    public void setC(double c) {
        this.c = c;
    }
}
