package server;

import java.io.DataOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;

public class SimpleServer {

    //简单Socket服务方

    public static void main(String [] args) {
        ServerSocket s = null;
        try {
            s = new ServerSocket(5432);
        } catch (IOException e) {
            e.printStackTrace();
        }
        while (true) {
            try {
                Socket s1 = s.accept();//j建立socket,等待链接
                OutputStream os = s1.getOutputStream();
                DataOutputStream dos = new DataOutputStream(os);//输出
                dos.writeUTF("hello Server");//发送
                System.out.println("a client is connected");
                s1.close();//关闭链接
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
