﻿using BLL;
using Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class CarList : System.Web.UI.Page
{
    CarBLL cb = new CarBLL();
    BrandBLL bb = new BrandBLL();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            bindCars();
        }

    }

    private void bindCars()
    {
        gvCars.DataSource = cb.SelectCarPro();
        gvCars.DataBind();
    }
    public List<Brand> bindBrands()
    {
        try
        {
            return bb.GetBrands();
        }
        catch (Exception ex)
        {

            throw ex;
        }
    }


    protected void gvCars_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvCars.PageIndex = e.NewPageIndex;
        bindCars();
    }

    protected void gvCars_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            //高亮
          
            e.Row.Attributes.Add("onmouseover", "cur = this.style.backgroundColor; this.style.backgroundColor = '#66ffff';");
            e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor = cur;");
            Button b = new Button();
            b = (Button)e.Row.Cells[6].FindControl("Button1");
            if (e.Row.RowState==DataControlRowState.Normal|| e.Row.RowState == DataControlRowState.Alternate)
            {
                b.Enabled = true;
                string carId = ((Label)e.Row.Cells[1].FindControl("Label1")).Text;
                b.Attributes.Add("onclick", "return confirm('确定要删除吗？" + carId + "')");
            }
            else
            {
                b.Enabled = false;
            }

        }
    }

    protected void gvCars_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        try
        {
           
            int carId = (int)this.gvCars.DataKeys[e.RowIndex].Value;
            cb.DeleteCarByProId(carId);
            bindCars();
        }
        catch (Exception ex)
        {

            throw ex;
        }
    }

 

    protected void btnDelCars_Click(object sender, EventArgs e)
    {
        for (int i = 0; i < gvCars.Rows.Count; i++)
        {
            if (((CheckBox)gvCars.Rows[i].Cells[0].FindControl("CheckBox1")).Checked==true)
            {
                int carId = (int)this.gvCars.DataKeys[i].Value;
                cb.DeleteCarByProId(carId);
            }
        }
        bindCars();
    }
  public bool  boolPic(FileUpload ful)
    {
        if (ful.HasFile)
        {
            //项目中的图片文件
            string path = Server.MapPath("~/images/");
            string[] exts = { ".gif", ".png", ".jpg", ".bmp", ".jpeg" };
            string fileExtend = Path.GetExtension(ful.FileName);
            if (exts.Contains(fileExtend))
            {
                ful.PostedFile.SaveAs(path + ful.FileName);
                return true;
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "a", "alert('请文件格式不正确')", true);
                return false;
            }
        }
        else
        {
            ScriptManager.RegisterStartupScript(this,this.GetType(),"a","alert('请选择文件')",true);
            return false;
        }
      

    }
    protected void gvCars_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
       
        FileUpload ful = ((FileUpload)gvCars.Rows[e.RowIndex].FindControl("FileUpload1"));
        if (boolPic(ful))
        {
            Car car = new Car();
            car.CarId= (int)this.gvCars.DataKeys[e.RowIndex].Value;
            car.BrandId = Convert.ToInt32(((DropDownList)gvCars.Rows[e.RowIndex].FindControl("DropDownList1")).SelectedValue);
            car.CarName = ((TextBox)gvCars.Rows[e.RowIndex].FindControl("TextBox1")).Text;
            car.Picture = ful.FileName;
            car.OfficialPrice =Convert.ToInt32(((TextBox)gvCars.Rows[e.RowIndex].FindControl("TextBox2")).Text);
            if (cb.UpdateCarPro(car))
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "a", "alert('修改成功')", true);
                gvCars.EditIndex = -1;
                bindCars();
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "a", "alert('修改失败')", true);
            }
        }
       
      
       

    }

    protected void gvCars_RowEditing(object sender, GridViewEditEventArgs e)
    {
        //编辑状态
        gvCars.EditIndex = e.NewEditIndex;
        bindCars();
    }

    protected void gvCars_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        gvCars.EditIndex =-1;
        bindCars();
    }
}