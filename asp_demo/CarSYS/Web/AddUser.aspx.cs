﻿using BLL;
using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class AddUser : System.Web.UI.Page
{
    UserBLL ub = new UserBLL();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack) {
            txtDate.Attributes.Add("onclick", "WdatePicker()");
        }
    }

    protected void CustomValidator1_ServerValidate(object source, ServerValidateEventArgs args)
    {
        
        try
        {
            User u = ub.Select(txtUserName.Text);
            if (u == null)
            {
                args.IsValid = true;
            }
            else
            {
                args.IsValid = false;
                CustomValidator1.ErrorMessage = "用户已经存在";

            }
        }
        catch (Exception ex)
        {

            lblinfo.Text = ex.Message;
        }
       
    }

    protected void btnAdd_Click(object sender, EventArgs e)
    {
    if (Page.IsValid)
    {
        lblinfo.Text = "";
        User user = new User();
        user.LoginName = txtUserName.Text;
        user.LoginPwd = txtUserPwd.Text;
        user.BornDate =Convert.ToDateTime( txtDate.Text);
        user.Email = txtEmail.Text;
            //if (ub.AddCar(user)) {
            //    User use = ub.Select(txtUserName.Text);
            //    lblinfo.Text = "添加成功，新用户的编号为：" + use.ID;
            //}
            if (ub.AddUserPro(user)) {
                lblinfo.Text = "添加成功，新用户的编号为：" + user.ID;
            }
        else
        {
            lblinfo.Text = "添加失败";
        }
        }
    }
}