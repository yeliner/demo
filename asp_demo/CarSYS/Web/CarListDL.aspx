﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master.master" AutoEventWireup="true" CodeFile="CarListDL.aspx.cs" Inherits="CarListDL" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cph1" Runat="Server">
    <asp:DataList ID="dlCar" runat="server" RepeatColumns="3" RepeatDirection="Horizontal">
       
        <ItemTemplate>
             <table border="1"  style="width:250px;height:80px;margin:10px auto;border-collapse:collapse">
            <tr ><td colspan="2">
                <asp:Label ID="CarName" runat="server" Text='<%# Eval("CarName") %>'></asp:Label>
                 </td>
            </tr>
             <tr><td rowspan="4" >
                 <asp:Image Width="100px" Height="80px" ID="Image1" runat="server" ImageUrl='<%# Eval("Picture","~/images/{0}") %>'  /></td></tr>
             <tr><td> <asp:Label ID="CarId" runat="server" Text='<%# Eval("CarId") %>'></asp:Label></td></tr>
             <tr><td> <asp:Label ID="BrandName" runat="server" Text='<%# Eval("BrandName") %>'></asp:Label></td></tr>
             <tr><td> <asp:Label ID="OfficialPrice" runat="server" Text='<%# Eval("OfficialPrice") %>'></asp:Label></td></tr>
                  </table>
        </ItemTemplate>
        <SeparatorTemplate><hr />
        </SeparatorTemplate>
        
    </asp:DataList>
</asp:Content>

