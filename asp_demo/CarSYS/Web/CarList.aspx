﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master.master" AutoEventWireup="true" CodeFile="CarList.aspx.cs" Inherits="CarList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <script type="text/javascript">
        function checkAll(o) {
            var checkBox = document.getElementsByTagName("input");
            for (var i = 0; i < checkBox.length; i++) {
                if (checkBox[i].type == "checkbox") {
                    checkBox[i].checked = o.checked;
                }
            }
        }
    </script>
   
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cph1" Runat="Server">
    车型列表<br /><br />
    <asp:GridView ID="gvCars" runat="server" AutoGenerateColumns="False" HorizontalAlign="Center" AllowPaging="True"  OnPageIndexChanging="gvCars_PageIndexChanging" PageSize="5" OnRowDataBound="gvCars_RowDataBound" OnRowDeleting="gvCars_RowDeleting" DataKeyNames="CarId" OnRowEditing="gvCars_RowEditing" OnRowUpdating="gvCars_RowUpdating" OnRowCancelingEdit="gvCars_RowCancelingEdit" CellPadding="4" ForeColor="#333333" GridLines="None" Width="665px" Height="373px">
        <AlternatingRowStyle BackColor="White" />
        <Columns>
            <asp:TemplateField HeaderText="全选">
                <HeaderTemplate>
                    <input type="checkbox" id="CheckBox2" onchange="checkAll(this)"   runat="server"   />
                </HeaderTemplate>
                <ItemTemplate>
                    <asp:CheckBox ID="CheckBox1" runat="server" />
                </ItemTemplate>
                <ItemStyle HorizontalAlign="Center" Height="60px" />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="车型名称">
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("CarName") %>'></asp:TextBox>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="Label1" runat="server" Text='<%# Bind("CarName") %>'></asp:Label>
                </ItemTemplate>
                <ItemStyle Height="60px" HorizontalAlign="Center" />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="品牌名称">
                <EditItemTemplate>
                    <asp:DropDownList ID="DropDownList1" runat="server" Text='<%# Bind("BrandName") %>' DataSource="<%# bindBrands() %>" DataValueField="BrandId" DataTextField="BrandName" SelectedValue='<%# Bind("BrandId") %>' ></asp:DropDownList>
                 </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="Label2" runat="server" Text='<%# Bind("BrandName") %>'></asp:Label>
                </ItemTemplate>
                <ItemStyle Height="60px" HorizontalAlign="Center" />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="官方价格">
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox2" runat="server" Text='<%# Bind("OfficialPrice") %>'></asp:TextBox>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="Label3" runat="server" Text='<%# Bind("OfficialPrice", "{0:C}万") %>'></asp:Label>
                </ItemTemplate>
                <ItemStyle Height="60px" HorizontalAlign="Center" />
            </asp:TemplateField>
            <asp:BoundField DataField="Click" HeaderText="点击量" ReadOnly="True">
            <ItemStyle HorizontalAlign="Center" />
            </asp:BoundField>
            <asp:TemplateField HeaderText="实物图">
                <EditItemTemplate>
                    <asp:FileUpload ID="FileUpload1" runat="server" />
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Image ID="Image1" runat="server" ImageUrl='<%# Eval("Picture", "~/images/{0}") %>' />
                </ItemTemplate>
                <ControlStyle Height="60px" Width="80px" />
                <ItemStyle HorizontalAlign="Center" />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="删除" ShowHeader="False">
                <ItemTemplate>
                    <asp:Button ID="Button1" runat="server" CausesValidation="False" CommandName="Delete" Text="删除" />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:HyperLinkField DataNavigateUrlFields="CarId" DataNavigateUrlFormatString="~/DetailCar.aspx?CarId={0}" HeaderText="详细" Text="详细">
            <ItemStyle HorizontalAlign="Center" />
            </asp:HyperLinkField>
            <asp:CommandField ButtonType="Button" HeaderText="编辑" ShowEditButton="True" />
        </Columns>
        <EditRowStyle BackColor="#2461BF" />
        <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
        <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
        <PagerStyle HorizontalAlign="Center" BackColor="#2461BF" ForeColor="White" />
        <RowStyle BackColor="#EFF3FB" />
        <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
        <SortedAscendingCellStyle BackColor="#F5F7FB" />
        <SortedAscendingHeaderStyle BackColor="#6D95E1" />
        <SortedDescendingCellStyle BackColor="#E9EBEF" />
        <SortedDescendingHeaderStyle BackColor="#4870BE" />
    </asp:GridView>
    <asp:Button ID="btnDelCars" runat="server" Text="删除多行" OnClick="btnDelCars_Click" />
</asp:Content>

