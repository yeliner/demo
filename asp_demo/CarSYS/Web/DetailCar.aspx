﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master.master" AutoEventWireup="true" CodeFile="DetailCar.aspx.cs" Inherits="DetailCar" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cph1" Runat="Server">
    <strong> 详细信息如下显示</strong>
    <asp:DetailsView ID="dvCar" runat="server" Height="41px" Width="283px" AutoGenerateRows="False" OnModeChanging="dvCar_ModeChanging" OnPageIndexChanging="dvCar_PageIndexChanging" DataKeyNames="CarId" OnItemUpdating="dvCar_ItemUpdating" OnItemInserting="dvCar_ItemInserting">
        <Fields>
            <asp:TemplateField HeaderText="车型编号">
                <EditItemTemplate>
                   <asp:Label ID="Label1" runat="server" Text='<%# Bind("CarId") %>'></asp:Label>
                </EditItemTemplate>
                <InsertItemTemplate>
                   <%-- <asp:Label ID="Label1" runat="server" Text=''></asp:Label>--%>
                </InsertItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="Label1" runat="server" Text='<%# Bind("CarId") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="车型名称">
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox2" runat="server" Text='<%# Bind("CarName") %>'></asp:TextBox>
                </EditItemTemplate>
                <InsertItemTemplate>
                    <asp:TextBox ID="TextBox2" runat="server" Text='<%# Bind("CarName") %>'></asp:TextBox>
                </InsertItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="Label2" runat="server" Text='<%# Bind("CarName") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="品牌名称">
                <EditItemTemplate>
                    <asp:DropDownList ID="DropDownList1" SelectedValue='<%#Eval("BrandId") %>' DataSource="<%# getGrands() %>" DataValueField="BrandId" DataTextField="BrandName" runat="server"></asp:DropDownList>
                 </EditItemTemplate>
                <InsertItemTemplate> 
                    <asp:DropDownList ID="DropDownList1" DataSource="<%# getGrands() %>" DataValueField="BrandId" DataTextField="BrandName" runat="server"></asp:DropDownList>
                </InsertItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="Label3" runat="server" Text='<%# Bind("BrandName") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="实物图">
                <EditItemTemplate>
                       <asp:FileUpload ID="FileUpload1" runat="server" />
                </EditItemTemplate>
                <InsertItemTemplate>
                    <asp:FileUpload ID="FileUpload1" runat="server" />
                </InsertItemTemplate>
                <ItemTemplate>
                    <asp:Image ID="Image1" runat="server" ImageUrl='<%# Eval("Picture", "~/images/{0}") %>' />
                </ItemTemplate>
                <ControlStyle Height="50px" />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="官方价格">
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox6" runat="server" Text='<%# Bind("OfficialPrice") %>'></asp:TextBox>
                </EditItemTemplate>
                <InsertItemTemplate>
                    <asp:TextBox ID="TextBox6" runat="server" Text='<%# Bind("OfficialPrice") %>'></asp:TextBox>
                </InsertItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="Label5" runat="server" Text='<%# Bind("OfficialPrice") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="点击量" >
                <EditItemTemplate>
                    <asp:Label ID="Label4" runat="server" Text='<%# Bind("Click") %>'></asp:Label>
                </EditItemTemplate>
                <InsertItemTemplate> 
                    <asp:Label ID="Label4" runat="server" Text='0'></asp:Label>
                </InsertItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="Label4" runat="server" Text='<%# Bind("Click") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="编辑" ShowHeader="False">
                <EditItemTemplate>
                    <asp:Button ID="Button1" runat="server" CausesValidation="True" CommandName="Update" Text="更新" />
                    &nbsp;<asp:Button ID="Button2" runat="server" CausesValidation="False" CommandName="Cancel" Text="取消" />
                </EditItemTemplate>
                <InsertItemTemplate>
                    <asp:Button ID="Button1" runat="server" CausesValidation="True" CommandName="Insert" Text="插入" />
                    &nbsp;<asp:Button ID="Button2" runat="server" CausesValidation="False" CommandName="Cancel" Text="取消" />
                </InsertItemTemplate>
                <ItemTemplate>
                    <asp:Button ID="Button1" runat="server" CausesValidation="False" CommandName="Edit" Text="编辑" />
                    &nbsp;<asp:Button ID="Button2" runat="server" CausesValidation="False" CommandName="New" Text="新建" />
                </ItemTemplate>
            </asp:TemplateField>
        </Fields>
    </asp:DetailsView>
</asp:Content>

