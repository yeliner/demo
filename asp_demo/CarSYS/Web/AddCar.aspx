﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="AddCar.aspx.cs" Inherits="AddCar" MasterPageFile="~/Master.master" %>

<asp:Content ID="head" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        #add {
            margin-left:150px;
            margin-top:50px;
            border: 1px solid #000;
        }
    </style>

</asp:Content>
<asp:Content ID="Add" ContentPlaceHolderID="cph1" runat="server">
    <div id="add">
        <table width="487" height="234">
            <tr>
                <td >车型名称:</td>
                <td align="left">
                    <asp:TextBox ID="txtCarName" runat="server" Width="213px"></asp:TextBox>

                    <asp:RequiredFieldValidator ID="rfvCarName" runat="server" ErrorMessage="*" ControlToValidate="txtCarName" Display="Dynamic"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>品牌:</td>
                <td align="left">
                    <asp:DropDownList ID="ddlBrand" runat="server" Height="26px" Width="220px">
                    </asp:DropDownList>
                    
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="*" ControlToValidate="ddlBrand" Display="Dynamic" InitialValue="-1"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>实物图文件名:</td>
                <td align="left">
                    <asp:FileUpload ID="fulImg" runat="server" />
                       <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="*" ControlToValidate="fulImg" Display="Dynamic" ></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>官方价:</td>
                <td align="left">
                    <asp:TextBox ID="txtPrice" runat="server" Width="213px"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="*" ControlToValidate="txtPrice" Display="Dynamic"></asp:RequiredFieldValidator>
                    <asp:CompareValidator ID="CompareValidator1" runat="server" ErrorMessage="价格格式错误" ControlToValidate="txtPrice" Operator="DataTypeCheck" Type="Integer"></asp:CompareValidator>
                </td>
            </tr>
            <tr>
                <td colspan="2" align="center">
                    <asp:Button ID="btnAdd" runat="server" Text="添加" OnClick="btnAdd_Click" />
                    <asp:Label
                        ID="lblinfo" runat="server" Text=""></asp:Label>
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
