﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master.master" AutoEventWireup="true" CodeFile="AddBrand.aspx.cs" Inherits="AddBrand" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style>
        #add {
            width: 400px;
            height: 200px;
            margin: 50px auto;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cph1" runat="Server">
    <div id="add">
        <asp:Label ID="lbl" runat="server" Text="请输入品牌名称："></asp:Label>
        <br />
          <br />
        <asp:TextBox ID="txtBrandName" runat="server"></asp:TextBox>
          <br />
        <asp:RequiredFieldValidator ID="rfvName" runat="server" ErrorMessage="*" ControlToValidate="txtBrandName" Display="Dynamic"></asp:RequiredFieldValidator>
        <asp:CustomValidator ID="CustomValidator1" runat="server" ControlToValidate="txtBrandName"  OnServerValidate="CustomValidator1_ServerValidate" Display="Static"></asp:CustomValidator>
           <br />
        <asp:Button ID="btnAdd" runat="server" Text="增加" OnClick="btnAdd_Click" />
        <br />

        <asp:Label ID="lblinfo" runat="server" Text=""></asp:Label>

    </div>

</asp:Content>

