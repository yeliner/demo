﻿using BLL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class CarListDL : System.Web.UI.Page
{
    CarBLL cb = new CarBLL();
    BrandBLL bb = new BrandBLL();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            bindCars();
        }
    }
    private void bindCars()
    {
        dlCar.DataSource = cb.SelectCarPro();
        dlCar.DataBind();
    }
}