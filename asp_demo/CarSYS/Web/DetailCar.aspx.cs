﻿using BLL;
using Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class DetailCar : System.Web.UI.Page
{
    CarBLL cb = new CarBLL();
    BrandBLL bb = new BrandBLL();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            int carId = int.Parse(Request.QueryString["CarId"]);
            if (carId!=0)
            {
                bindcCar(carId);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "a", "alert('id为空')", true);
            }
           
        }
       
    }
    public List<Brand> getGrands()
    {
        List<Brand> brands = new List<Brand>();
        brands = bb.GetBrands();
        return brands;
    }
    

    private void bindcCar(int carId)
    {
        dvCar.DataSource = cb.SelectCarProByCid(carId);
        dvCar.DataBind();
    }

    protected void dvCar_PageIndexChanging(object sender, DetailsViewPageEventArgs e)
    {
       
    }

    protected void dvCar_ModeChanging(object sender, DetailsViewModeEventArgs e)
    {
        dvCar.ChangeMode(e.NewMode);
        int cId = Convert.ToInt32(dvCar.DataKey.Value);
        bindcCar(cId);
    }

    protected void dvCar_ItemUpdating(object sender, DetailsViewUpdateEventArgs e)
    {
        FileUpload ful = ((FileUpload)dvCar.FindControl("FileUpload1"));
        if (boolPic(ful))
        {
            Car car = new Car();
            car.CarId = (int)this.dvCar.DataKey.Value;
            car.BrandId = Convert.ToInt32(((DropDownList)dvCar.FindControl("DropDownList1")).SelectedValue);
            car.CarName = ((TextBox)dvCar.FindControl("TextBox2")).Text;
            car.Picture = ful.FileName;
            car.OfficialPrice = Convert.ToInt32(((TextBox)dvCar.FindControl("TextBox6")).Text);
            if (cb.UpdateCarPro(car))
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "a", "alert('修改成功')", true);
                dvCar.ChangeMode(DetailsViewMode.ReadOnly);
                bindcCar(car.CarId); 
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "a", "alert('修改失败')", true);
            }
        }
    }
    public bool boolPic(FileUpload ful)
    {
        if (ful.HasFile)
        {
            //项目中的图片文件
            string path = Server.MapPath("~/images/");
            string[] exts = { ".gif", ".png", ".jpg", ".bmp", ".jpeg" };
            string fileExtend = Path.GetExtension(ful.FileName);
            if (exts.Contains(fileExtend))
            {
                ful.PostedFile.SaveAs(path + ful.FileName);
                return true;
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "a", "alert('请文件格式不正确')", true);
                return false;
            }
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "a", "alert('请选择文件')", true);
            return false;
        }


    }

    protected void dvCar_ItemInserting(object sender, DetailsViewInsertEventArgs e)
    {
        FileUpload ful = ((FileUpload)dvCar.FindControl("FileUpload1"));
        if (boolPic(ful))
        {
            Car car = new Car();
            car.BrandId = Convert.ToInt32(((DropDownList)dvCar.FindControl("DropDownList1")).SelectedValue);
            car.CarName = ((TextBox)dvCar.FindControl("TextBox2")).Text;
            car.Picture = ful.FileName;
            car.OfficialPrice = Convert.ToInt32(((TextBox)dvCar.FindControl("TextBox6")).Text);
            car.Click= Convert.ToInt32(((Label)dvCar.FindControl("Label4")).Text);
            if (cb.AddCarPro(car))
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "a", "alert('新建成功')", true);
                dvCar.ChangeMode(DetailsViewMode.ReadOnly);
                bindcCar(car.CarId);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "a", "alert('新建失败')", true);
            }
        }
    }
}