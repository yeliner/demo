﻿using BLL;
using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class AddBrand : System.Web.UI.Page
{
    BrandBLL bb = new BrandBLL();
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void CustomValidator1_ServerValidate(object source, ServerValidateEventArgs args)
    {
        try
        {
            List<Brand> brands = bb.GetBrands();
            args.IsValid = true;
            foreach (Brand item in brands)
            {
                if (item.BrandName.Equals(txtBrandName.Text)) {
                    args.IsValid = false;
                    CustomValidator1.ErrorMessage = "车牌名已经存在";
                    break;
                }
            }
         
        }
        catch (Exception ex)
        {

            lblinfo.Text = ex.Message;
        }
    }

    protected void btnAdd_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            lblinfo.Text = "";
            Brand brand = new Brand();
            brand.BrandName = txtBrandName.Text;
            //if (ub.AddCar(user)) {
            //    User use = ub.Select(txtUserName.Text);
            //    lblinfo.Text = "添加成功，新用户的编号为：" + use.ID;
            //}
            if (bb.AddBrandPro(brand))
            {
                lblinfo.Text = "添加成功，新车名的编号为：" + brand.BrandId;
            }
            else
            {
                lblinfo.Text = "添加失败";
            }
        }
    }
}