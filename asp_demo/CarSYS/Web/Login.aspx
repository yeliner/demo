﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Login.aspx.cs" Inherits="Login" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        
        <table style="margin: inherit; border: 1px solid #C0C0C0; vertical-align: middle;" >
            <tr>
                <td colspan="2" align="center" style="background-color: #00FFFF;">请登录  </td>

            </tr>
            <tr>
                <td  >用户名:</td>
                <td align="left">
                    <asp:TextBox ID="txtUserName" runat="server" Width="163px"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvUserName" runat="server" ErrorMessage="用户名不能为空" ControlToValidate="txtUserName" Display="Dynamic"></asp:RequiredFieldValidator>
                    
                </td>
            </tr>
            <tr>
                <td >密码:</td>
                <td align="left">
                    <asp:TextBox ID="txtUserPwd" runat="server" Width="163px"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvUserPwd" runat="server" ErrorMessage="密码不能为空" ControlToValidate="txtUserPwd"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td colspan="2" align="center">
                    <asp:Button ID="btnLogin" runat="server" Text="登录" OnClick="btnLogin_Click" />
                    <asp:CheckBox ID="cbsave" runat="server" />保存记录
                </td>

            </tr>

        </table>
    </form>
</body>
</html>
