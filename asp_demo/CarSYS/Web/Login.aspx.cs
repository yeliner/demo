﻿using BLL;
using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;

public partial class Login : System.Web.UI.Page
{

    UserBLL ub = new UserBLL();
    protected void Page_Load(object sender, EventArgs e)
    {
       
    }

    protected void btnLogin_Click(object sender, EventArgs e)
    {
        string name = txtUserName.Text;
        string pwd = txtUserPwd.Text;
        if (ub.SelectUserByNameAndPwd(name, pwd))
        {
            //Session["UserName"] = name;
            //Response.Redirect("Default.aspx");
            string strRed = Request["ReturnUrl"];
            System.Web.Security.FormsAuthentication.SetAuthCookie(name, cbsave.Checked);
            if (strRed == null)
            {
                Response.Redirect("~/Default.aspx");
            }
            else
            {
                Response.Redirect(strRed);
            }
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "xx", "alert('登录失败，请重新输入用户名或者密码')", true);

        }
    }
}