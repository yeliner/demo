﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Model;
using BLL;
using System.IO;

public partial class AddCar : System.Web.UI.Page
{
    BrandBLL bb = new BrandBLL();
    CarBLL cb = new CarBLL();
    protected void Page_Load(object sender, EventArgs e)
    {
        
        if (!IsPostBack)
        {
            try
            {
                ddlBrand.DataSource = bb.GetBrands();
                ddlBrand.DataTextField = "BrandName";
                ddlBrand.DataValueField = "BrandId";
                ddlBrand.DataBind();
                ddlBrand.SelectedIndex = 0;
            }
            catch (Exception ex)
            {
                Response.Write(ex.Message);
            }
            ddlBrand.Items.Insert(0, new ListItem("请选择", "-1"));

        }
    }
    protected void btnAdd_Click(object sender, EventArgs e)
    {
        string path = Server.MapPath("~/images/");
        bool isPic = false;
        string[] exts = { ".gif", ".png", ".jpg", ".bmp", ".jpeg" };
        string fileExtend = Path.GetExtension(fulImg.FileName);
        if (exts.Contains(fileExtend))
        {
            isPic = true;
           
        }
        if (isPic) {

            if (!File.Exists(path + fulImg.FileName))
            {
                fulImg.PostedFile.SaveAs(path + fulImg.FileName);

                Car car = new Car();
                car.BrandId = int.Parse(ddlBrand.SelectedValue);
                car.CarName = txtCarName.Text;
                car.Picture = fulImg.FileName;
                car.OfficialPrice = int.Parse(txtPrice.Text);
                //if (cb.AddCar(car))
                //{
                //    Car nCar = cb.GetCar(txtCarName.Text, int.Parse(ddlBrand.SelectedValue));
                //    lblinfo.Text = "添加成功!车型的编号是:" + nCar.CarId;
                //}
                if (cb.AddCarPro(car))
                {
                    lblinfo.Text = "添加成功，新车型的编号为：" + car.CarId;
                }
                else
                {

                    lblinfo.Text = "添加失败!";
                }
            }
            else {
                lblinfo.Text = "实物图已经存在";
            }
        }
        else
        {
            lblinfo.Text = "只能传图像文件";
        }
     
    }
}