﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master.master" AutoEventWireup="true" CodeFile="CarListRepeater.aspx.cs" Inherits="CarListRepeater" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cph1" runat="Server">
    <asp:Repeater ID="reCars" runat="server" OnItemCommand="reCars_ItemCommand">
        <HeaderTemplate>
            <table  border="1" style="border-collapse:collapse">
                <tr>
                    <td>车型名称</td>
                     <td>品牌</td>
                     <td>官方价格</td>
                     <td>点击量</td>
                     <td>实物图</td>
                     <td>操作</td>
                </tr>
        </HeaderTemplate>
        <ItemTemplate>
              <tr>
                    <td><%# Eval("CarName") %></td>
                     <td><%# Eval("BrandName") %></td>
                     <td><%# Eval("OfficialPrice") %></td>
                     <td><%# Eval("Click") %></td>
                     <td><asp:Image Width="80px" Height="50px" ID="Image1" runat="server" ImageUrl='<%# Eval("Picture","~/images/{0}") %>'  /></td>
                     <td>
                         <asp:Button ID="btndel" runat="server" Text="删除" CommandArgument='<%# Eval("CarId") %>' CommandName="del"/>&nbsp;
                         <asp:Button ID="Button2" runat="server" Text="编辑" CommandArgument='<%# Eval("CarId") %>' CommandName="edit" /></td>
                </tr>
        </ItemTemplate>
        <FooterTemplate>
            </table>
        </FooterTemplate>
    </asp:Repeater>
</asp:Content>

