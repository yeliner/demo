﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master.master" AutoEventWireup="true" CodeFile="AddUser.aspx.cs" Inherits="AddUser" %>

<asp:Content ID="head" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript" src="WdatePicker/My97DatePicker/WdatePicker.js"></script>
    <style type="text/css">
        #table {
            margin: 100px;
        }

        .auto-style1 {
            width: 102px;
        }

        .auto-style2 {
            width: 102px;
            height: 50px;
        }

        .auto-style3 {
            height: 50px;
        }
    </style>
    <script type="text/javascript">
        function checkUserName(src,args){
            var falg = args.Value.length >= 3 && args.Value.length <= 50;
            args.IsValid = falg;

        }


    </script>
</asp:Content>
<asp:Content ID="Add" ContentPlaceHolderID="cph1" runat="server">
    <div id="table">
        <table height="134" width="585" style="border: 1px solid #000;">
            <tr>
                <td class="auto-style1">用户名:</td>
                <td align="left">
                    <asp:TextBox ID="txtUserName" runat="server" Width="163px"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvUserName" runat="server" ErrorMessage="用户名不能为空" ControlToValidate="txtUserName" Display="Dynamic"></asp:RequiredFieldValidator>
                    <asp:CustomValidator ID="CustomValidator1" runat="server" ErrorMessage="用户名的长度在3-50之间" ControlToValidate="txtUserName" ClientValidationFunction="checkUserName" OnServerValidate="CustomValidator1_ServerValidate" Display="Dynamic"></asp:CustomValidator>
                </td>
            </tr>
            <tr>
                <td class="auto-style1">密码:</td>
                <td align="left">
                    <asp:TextBox ID="txtUserPwd" runat="server" Width="163px"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvUserPwd" runat="server" ErrorMessage="密码不能为空" ControlToValidate="txtUserPwd"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td class="auto-style1">确认密码:</td>
                <td align="left">
                    <asp:TextBox ID="txtRePwd" runat="server" Width="163px" Height="16px"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvRePwd" runat="server" ErrorMessage="确认密码不能为空" ControlToValidate="txtRePwd" Display="Dynamic"></asp:RequiredFieldValidator>
                    <asp:CompareValidator ID="CompareValidator1" runat="server" ErrorMessage="密码不一致" ControlToValidate="txtRePwd" ControlToCompare="txtUserPwd" Operator="Equal" Display="Dynamic"></asp:CompareValidator>
                </td>
            </tr>
            <tr>
                <td class="auto-style1">出生日期:</td>
                <td align="left">
                    <asp:TextBox ID="txtDate" runat="server" Width="163px"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvDate" runat="server" ErrorMessage="出生日期不能为空" ControlToValidate="txtDate" Display="Dynamic"></asp:RequiredFieldValidator>
                    <asp:RangeValidator ID="RangeValidator1" runat="server" ErrorMessage="出生日期在1966-1-1到1997-12-31" MaximumValue="1997-12-31" MinimumValue="1966-01-01" Type="Date" ControlToValidate="txtDate" Display="Dynamic"></asp:RangeValidator>
                </td>
            </tr>
            <tr>
                <td class="auto-style2">邮箱:</td>
                <td align="left" class="auto-style3">
                    <asp:TextBox ID="txtEmail" runat="server" Width="163px"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvEmail" runat="server" ErrorMessage="邮箱不能为空" ControlToValidate="txtEmail" Display="Dynamic"></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ErrorMessage="邮箱格式有误"  ControlToValidate="txtEmail" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" Display="Dynamic"></asp:RegularExpressionValidator>
                </td>
            </tr>
            <tr>
                <td colspan="2" align="center">
                    <asp:Button ID="btnAdd" runat="server" Text="添加" OnClick="btnAdd_Click" />
                    <asp:Label
                        ID="lblinfo" runat="server" Text=""></asp:Label>
                </td>
            </tr>
        </table>
    </div>
</asp:Content>


