﻿using BLL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class CarListRepeater : System.Web.UI.Page
{
    CarBLL cb = new CarBLL();
    BrandBLL bb = new BrandBLL();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            bindCars();
        }
    }
    private void bindCars()
    {
        reCars.DataSource = cb.SelectCarPro();
        reCars.DataBind();
    }

    protected void reCars_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        bool b;
      int carId= Convert.ToInt32(e.CommandArgument);
        if (e.CommandName=="del")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "a", "alert('删除的车型编号是" + carId + "')", true);
            b=cb.DeleteCarByProId(carId);
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "a", "alert('修改的车型编号是" + carId + "')", true);
            Response.Redirect("DetailCar.aspx?CarId="+carId);
        }
    }
}