﻿using System;
using System.Data;
using System.Data.SqlClient;

using System.Configuration;
public class DataHelper
{
    //public static string constr = "server=.;uid=sa;pwd=123.com;database=CarSYS";
    public static readonly string constr = ConfigurationManager.ConnectionStrings["CarSYS"].ConnectionString;
    public static SqlConnection con = new SqlConnection(constr);

    public static bool doexecuteNonQuery(string sql)
    {
        int i = 0;
        try
        {
            con.Open();
            SqlCommand com = new SqlCommand(sql, con);
            i = com.ExecuteNonQuery();
        }
        catch (Exception)
        {

            Console.WriteLine("访问数据库失败！");
        }
        finally
        {
            con.Close();
        }
        return i > 0;
    }
    public static DataSet executeDataSet(string sql)
    {
        DataSet ds = null;
        try
        {
            ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter(sql, con);
            da.Fill(ds);
        }
        catch (Exception)
        {

            Console.WriteLine("访问数据库失败！");
        }
        return ds;
    }
}
