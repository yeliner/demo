﻿using Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace DAL
{
    public class UserDAL
    {
        public bool AddUser(User user)
        {
            string sql = string.Format("  insert into Users values('{0}','{1}','{2}','{3}')",
               user.LoginName, user.LoginPwd, user.BornDate, user.Email);
            return DataHelper.doexecuteNonQuery(sql);
        }
        public User Select(string LoginName)
        {
            User user = new User();
            string sql = string.Format("Select * from Users where LoginName='{0}'", LoginName);
            DataSet ds = DataHelper.executeDataSet(sql);
            user.LoginName = Convert.ToString(ds.Tables[0].Rows[0]["LoginName"]);
            user.LoginPwd = Convert.ToString(ds.Tables[0].Rows[0]["LoginPwd"]);
            user.BornDate = Convert.ToDateTime(ds.Tables[0].Rows[0]["BornDate"]);
            user.Email = Convert.ToString(ds.Tables[0].Rows[0]["Email"]);
            return user;
        }
      public bool SelectUserByNameAndPwd(string loginName,string LoginPwd)
        {
            int i;
            SqlConnection con = DataHelper.con;
            con.Open();
            try
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = con;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "usp_SelectCountByNameAndPwd";
                SqlParameter[] para = new SqlParameter[]
              {
                    cmd.Parameters.Add(new SqlParameter ("@LoginName ",loginName)),
                      cmd.Parameters.Add(new SqlParameter ("@LoginPwd ",LoginPwd)),
              };
                i=Convert.ToInt32( cmd.ExecuteScalar());
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                con.Close();
            }
            return i>0;
        }
        public bool AddUserPro(User user)
        {
            SqlConnection con = DataHelper.con;
            con.Open();
            try
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = con;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "usp_AddUser";
                SqlParameter[] para = new SqlParameter[]
                {
                cmd.Parameters.Add(new SqlParameter ("@LoginName",user.LoginName)),
                  cmd.Parameters.Add(new SqlParameter ("@LoginPwd",user.LoginPwd)),
                 cmd.Parameters.Add(new SqlParameter ("@BornDate",user.BornDate)),
                  cmd.Parameters.Add(new SqlParameter ("@Email",user.Email)),
                   cmd.Parameters.Add(new SqlParameter ("@ID",SqlDbType.Int)),

                };
                cmd.Parameters["@ID"].Direction = ParameterDirection.Output;
                cmd.ExecuteNonQuery();
                user.ID = Convert.ToInt32(cmd.Parameters["@ID"].Value);
            }
            catch (Exception ex)
            {
               
                throw ex;
            }
            finally {
                con.Close();
            }
            return user.ID > 0;
        }
    }
}
