﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Model;
using System.Data;
using System.Data.SqlClient;

namespace DAL
{
  public  class BrandDAL
    {
      public List<Brand> GetBrands() {
          string sql = "select * from Brand";
          DataSet ds = DataHelper.executeDataSet(sql);
          List<Brand> brands =new List<Brand> ();
          foreach (DataRow dr in ds.Tables[0].Rows)
          {
              Brand brand = new Brand();
              brand.BrandId = Convert.ToInt32(dr["BrandId"]);
              brand.BrandName = Convert.ToString(dr["BrandName"]);
              brands.Add(brand);
          }
          return brands;
      }

        public bool AddBrandPro(Brand brand)
        {
            SqlConnection con = DataHelper.con;
            con.Open();
            try
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = con;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "usp_AddBrand";
                SqlParameter[] para = new SqlParameter[]
                {
                cmd.Parameters.Add(new SqlParameter ("@BrandName",brand.BrandName)),
                
                   cmd.Parameters.Add(new SqlParameter ("@ID",SqlDbType.Int)),

                };
                cmd.Parameters["@ID"].Direction = ParameterDirection.Output;
                cmd.ExecuteNonQuery();
                brand.BrandId= Convert.ToInt32(cmd.Parameters["@ID"].Value);
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                con.Close();
            }
            return brand.BrandId > 0;
        }


    }
}
