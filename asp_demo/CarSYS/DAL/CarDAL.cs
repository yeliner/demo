﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Model;
using System.Data;
using System.Data.SqlClient;

namespace DAL
{
    public class CarDAL
    {
        //添加车辆
       
        public List<Car> SelectCarPro()
        {
            List<Car> cars = new List<Car>();
            SqlConnection con = DataHelper.con;
            con.Open();
            try
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = con;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "usp_selectCar";
                SqlDataAdapter sda = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                sda.Fill(ds);
                foreach (DataRow item in ds.Tables[0].Rows)
                {
                    Car car = new Car();
                    car.BrandId = Convert.ToInt32(item["Bid"]);
                    car.CarId = Convert.ToInt32(item["CarId"]);
                    car.CarName = Convert.ToString(item["CarName"]);
                    car.Click = Convert.ToInt32(item["Click"]);
                    car.OfficialPrice = Convert.ToInt32(item["OfficialPrice"]);
                    car.Picture = Convert.ToString(item["Picture"]);
                    car.BrandName = item["BrandName"].ToString();
                    cars.Add(car);

                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                con.Close();
            }
            return cars;
        }
        public List<Car> SelectCarById(int CarId)
        {
            List<Car> cars = new List<Car>();
            SqlConnection con = DataHelper.con;
            con.Open();
            try
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = con;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "usp_SelectCarById";
                SqlParameter[] para = new SqlParameter[]
              {
                    cmd.Parameters.Add(new SqlParameter ("@carId ",CarId)),
              };
                SqlDataAdapter sda = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                sda.Fill(ds);
                foreach (DataRow item in ds.Tables[0].Rows)
                {
                    Car car = new Car();
                    car.BrandId = Convert.ToInt32(item["Bid"]);
                    car.CarId = Convert.ToInt32(item["CarId"]);
                    car.CarName = Convert.ToString(item["CarName"]);
                    car.Click = Convert.ToInt32(item["Click"]);
                    car.OfficialPrice = Convert.ToInt32(item["OfficialPrice"]);
                    car.Picture = Convert.ToString(item["Picture"]);
                    car.BrandName = item["BrandName"].ToString();
                    cars.Add(car);

                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                con.Close();
            }
            return cars;
        }
        
        public bool AddCarPro(Car car)
        {
            SqlConnection con = DataHelper.con;
            con.Open();
            try
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = con;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "usp_AddCar";
                SqlParameter[] para = new SqlParameter[]
                {
                cmd.Parameters.Add(new SqlParameter ("@CarName",car.CarName)),
                  cmd.Parameters.Add(new SqlParameter ("@BrandId",car.BrandId)),
                 cmd.Parameters.Add(new SqlParameter ("@Picture",car.Picture)),
                  cmd.Parameters.Add(new SqlParameter ("@OfficialPrice",car.OfficialPrice)),
                    cmd.Parameters.Add(new SqlParameter ("@Click",car.Click)),
                   cmd.Parameters.Add(new SqlParameter ("@carId",SqlDbType.Int)),

                };
                cmd.Parameters["@carId"].Direction = ParameterDirection.Output;
                cmd.ExecuteNonQuery();
                car.CarId = Convert.ToInt32(cmd.Parameters["@carId"].Value);
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                con.Close();
            }
            return car.CarId > 0;
        }
        public bool UpdateCarPro(Car car)
        {
            SqlConnection con = DataHelper.con;
            con.Open();
            int i;
            try
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = con;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "usp_UpdateCar";
                SqlParameter[] para = new SqlParameter[]
                {
                cmd.Parameters.Add(new SqlParameter ("@CarName",car.CarName)),
                  cmd.Parameters.Add(new SqlParameter ("@BrandId",car.BrandId)),
                 cmd.Parameters.Add(new SqlParameter ("@Picture",car.Picture)),
                  cmd.Parameters.Add(new SqlParameter ("@OfficialPrice",car.OfficialPrice)),
                   cmd.Parameters.Add(new SqlParameter ("@carId",car.CarId)),
                };
             i=  cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                con.Close();
            }
            return i > 0;
        }
        public bool DeleteCarByProId(int CarId)
        {
            int i;
            SqlConnection con = DataHelper.con;
            con.Open();
            try
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = con;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "usp_deleteCarById";
                SqlParameter[] para = new SqlParameter[]
                {
                    cmd.Parameters.Add(new SqlParameter ("@carId ",CarId)),
                };
               i= cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                con.Close();
            }
            return i> 0;
        }
        public bool Add(Car car)
        {
            string sql = string.Format("insert into Car values ('{0}',{1},'{2}',{3},1)",
                car.CarName, car.BrandId, car.Picture, car.OfficialPrice);
            return DataHelper.doexecuteNonQuery(sql);
        }
        public Car Select(string CarName, int BrandId)
        {
            Car car = new Car();
            string sql = string.Format("Select * from Car where CarName='{0}' and BrandId={1}", CarName, BrandId);
            DataSet ds = DataHelper.executeDataSet(sql);
            car.BrandId = Convert.ToInt32(ds.Tables[0].Rows[0][2]);
            car.CarId = Convert.ToInt32(ds.Tables[0].Rows[0]["CarId"]);
            car.CarName = Convert.ToString(ds.Tables[0].Rows[0]["CarName"]);
            car.Click = Convert.ToInt32(ds.Tables[0].Rows[0]["Click"]);
            car.OfficialPrice = Convert.ToInt32(ds.Tables[0].Rows[0]["OfficialPrice"]);
            car.Picture = Convert.ToString(ds.Tables[0].Rows[0]["Picture"]);
            return car;
        }
    }
}
