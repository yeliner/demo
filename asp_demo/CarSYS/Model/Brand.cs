﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Model
{
    [Serializable]
   public  class Brand
    {
     public int BrandId{get;set;}
     public string BrandName { get; set; }
    }
}
