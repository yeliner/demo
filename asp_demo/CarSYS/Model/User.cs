﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Model
{
    [Serializable]
    public class User
    {
        public int ID { get; set; }
        public string LoginName { get; set; }
        public string LoginPwd { get; set; }
        public DateTime BornDate { get; set; }
        public string Email { get; set; }
    }
}
