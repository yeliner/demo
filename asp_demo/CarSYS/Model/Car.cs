﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Model
{
    [Serializable]
    public class Car
    {
        public int CarId { get; set; }
        public string CarName { get; set; }
        public int BrandId { get; set; }
        public string Picture { get; set; }
        public int OfficialPrice { get; set; }
        public int Click { get; set; }
        public string BrandName { get; set; }

    }
}
