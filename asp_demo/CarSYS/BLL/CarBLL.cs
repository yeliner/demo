﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DAL;
using Model;

namespace BLL
{
  public   class CarBLL
    {
      CarDAL cd = new CarDAL();
     
        public List<Car> SelectCarPro()
        {
            try
            {
                return cd.SelectCarPro();
            }
            catch (Exception)
            {

                throw;
            }

        }
        public List<Car> SelectCarProByCid(int CarId)
        {
            try
            {
                return cd.SelectCarById(CarId);
            }
            catch (Exception)
            {

                throw;
            }

        }
        public bool DeleteCarByProId(int CarId)
        {

            try
            {
                return cd.DeleteCarByProId(CarId);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public bool UpdateCarPro(Car car)
        {

            try
            {
                return cd.UpdateCarPro(car);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        public bool AddCarPro(Car car)
        {

            try
            {
                return cd.AddCarPro(car);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        public Car GetCar(string CarName, int BrandId)
        {
            try
            {
                return cd.Select(CarName, BrandId);
            }
            catch (Exception)
            {

                throw;
            }
        }
        public bool AddCar(Car car)
        {

            try
            {
                return cd.Add(car);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
    }
}
