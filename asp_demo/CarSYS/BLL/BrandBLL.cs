﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DAL;
using Model;

namespace BLL
{
  public   class BrandBLL
    {
      BrandDAL bd = new BrandDAL();
      public List<Brand> GetBrands() {
          try
          {
              return bd.GetBrands();
          }
          catch (Exception ex)
          {
              
              throw ex;
          }
      }
        public bool AddBrandPro(Brand brand)
        {

            try
            {
                return bd.AddBrandPro(brand);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

    }
}
