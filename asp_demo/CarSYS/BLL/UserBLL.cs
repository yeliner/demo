﻿using DAL;
using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BLL
{
 public    class UserBLL
    {
        UserDAL ud = new UserDAL();
        public bool AddCar(User user)
        {

            try
            {
                 return ud.AddUser(user);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        public bool AddUserPro(User user)
        {

            try
            {
               return ud.AddUserPro(user);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        public bool SelectUserByNameAndPwd(string loginName, string LoginPwd)
        {
            try
            {
                return ud.SelectUserByNameAndPwd(loginName,LoginPwd);
            }
            catch (Exception)
            {

                throw;
            }
        }
       

        public User Select(string LoginName)
        {
            try
            {
                return ud.Select(LoginName);
            }
            catch (Exception)
            {

                throw;
            }
        }
    }
}
