package com.services;


import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;

@WebService(serviceName = "CalcService", targetNamespace = "mycalc")
@SOAPBinding(style = SOAPBinding.Style.RPC)
public interface CalcService {

	@WebResult(name = "out")
	public int add(@WebParam(name = "a", targetNamespace = "hpfcalc") int a,
			@WebParam(name = "b", targetNamespace = "hpfcalc") int b);
	
	@WebResult(name = "out")
	public int sub(@WebParam(name = "a", targetNamespace = "hpfcalc") int a,
			@WebParam(name = "b", targetNamespace = "hpfcalc") int b);

}
