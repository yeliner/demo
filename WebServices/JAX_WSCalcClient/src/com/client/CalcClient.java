package com.client;

import java.net.MalformedURLException;
import java.net.URL;

import javax.xml.namespace.QName;
import javax.xml.ws.Service;

import calc.Mycalc;

public class CalcClient {

	/**
	 * @param args
	 * @throws MalformedURLException 
	 */
	public static void main(String[] args) throws MalformedURLException {
		// TODO Auto-generated method stub
		QName serviceName = new QName("calc", "mycalc");
		QName portName = new QName("calc", "mycalcPort");
		String wsdl = "http://localhost:8090/calc?wsdl";
		try {
			URL wsdlurl=new URL(wsdl);
			Service service=Service.create(wsdlurl, serviceName);
			Mycalc calcservice=service.getPort(portName,Mycalc.class);
			double re=calcservice.add(11.5, 11.5);
			System.out.println(re);
		} catch (Exception e) {
			e.printStackTrace();
		}
		

	}

}
