package com.client;


import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.services.CalcService;

public class CalcClient {
	public CalcClient() {
	}

	public static void main(String[] args) {
		ApplicationContext ctx = new ClassPathXmlApplicationContext(
				"applicationContext.xml");
		CalcService calcService = (CalcService) ctx.getBean("calcClient");
		int i = calcService.add(33, 22);
		int j = calcService.sub(33, 22);
		System.out.println(i);
		System.out.println(j);

	}
}
