package com.servers;

import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;


@WebService(targetNamespace="calc")
@SOAPBinding(style= SOAPBinding.Style.RPC)
public interface CalcServices {
@WebMethod
public double add(Double a,Double b);
}
