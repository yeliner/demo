package com.servers;

import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;

@WebService(targetNamespace="calc",serviceName="mycalc",name="mycalc")

@SOAPBinding(style= SOAPBinding.Style.RPC)
public class CalcServiceImpl implements CalcServices {

	public double add(Double a, Double b) {
		// TODO Auto-generated method stub
		return a+b;
	}

}
